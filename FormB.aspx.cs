using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WebSupergoo.ABCpdf6;
using System.Text;

public partial class FormB : System.Web.UI.Page
{
    int UserID = 0, UID = 0;
    string UserType = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteValidation.ValidateSession();
        SiteValidation.ValidateSSL(Request.Url.AbsoluteUri);
        UserType = Session["UserType"].ToString();
        UserID = int.Parse(Session["UserID"].ToString());
        UID = int.Parse(Session["UID"].ToString());

        if (!Page.IsPostBack)
            BindData();

        lblMsg.Text = "";
        txtCommenceDate.Attributes.Add("onkeydown","return CheckDateEntry(this)");
        if (Session["UserType"].ToString() == "0")
            lnkBak.Visible = true;
        else
            lnkBak.Visible = false;

        lblUnionNo.Text = Session["UnionNo"].ToString();
        txtBankName.Focus();
    }
    private void BindData()
    {
        try
        {
            FormBClass objFrm = new FormBClass();
            objFrm.UserID = UserID;
            DataTable dtFrm = objFrm.GetFormB();

            if (dtFrm.Rows.Count > 0)
            {
                txtBankName.Value = dtFrm.Rows[0]["BankName"].ToString();
                txtAccNo.Value = dtFrm.Rows[0]["AccountNo"].ToString();
                txtSortCode.Value = dtFrm.Rows[0]["SortCode"].ToString();
                txtAddress1.Value = dtFrm.Rows[0]["BankAddress1"].ToString();
                txtAddress2.Value = dtFrm.Rows[0]["BankAddress2"].ToString();
                txtAddress3.Value = dtFrm.Rows[0]["BankAddress3"].ToString();
                txtAddress4.Value = dtFrm.Rows[0]["BankAddress4"].ToString();
                txtAcName1.Value = dtFrm.Rows[0]["AccountName1"].ToString();
                txtAcName2.Value = dtFrm.Rows[0]["AccountName2"].ToString();
                txtAccountType.Value = dtFrm.Rows[0]["AccoutType"].ToString();
                
                if (dtFrm.Rows[0]["Amount"].ToString() == "0.00")
                    txtAmount.Value = "";
                else
                    txtAmount.Value = dtFrm.Rows[0]["Amount"].ToString();
                
                txtAmtWords.Value = dtFrm.Rows[0]["AmountWords"].ToString();

                if (dtFrm.Rows[0]["CommenceDate"].ToString() == "1/1/1900" || dtFrm.Rows[0]["CommenceDate"].ToString() == "")
                    txtCommenceDate.Text = "";
                else
                    txtCommenceDate.Text = dtFrm.Rows[0]["CommenceDate"].ToString();

                if (dtFrm.Rows[0]["SaveAsPDF"].ToString() == "True")
                    chkSavePDF.Checked = true;
                else
                    chkSavePDF.Checked = false;
            }
            else
            {
                //DeclarationClass objDec = new DeclarationClass();
                //objDec.UserID = UserID;
                ////txtAcName1.Value = objDec.GetApplicantName(); 
                //objDec = null;
            }
            objFrm = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("Declaration.aspx");
    }

    protected void btnComplete_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("Success.aspx", false);
    }
    private void SaveData()
    {
        try
        {
            FormBClass objFrm = new FormBClass();
            objFrm.UserID = UserID;

            objFrm.BankName = txtBankName.Value;
            objFrm.AccountNo = txtAccNo.Value;
            objFrm.SortCode = txtSortCode.Value;
            objFrm.BankAddress1 = txtAddress1.Value;
            objFrm.BankAddress2 = txtAddress2.Value;
            objFrm.BankAddress3 = txtAddress3.Value;
            objFrm.BankAddress4 = txtAddress4.Value;
            objFrm.AccountName1 = txtAcName1.Value;
            objFrm.AccountName2 = txtAcName2.Value;
            objFrm.AccoutType = txtAccountType.Value;

            if (txtAmount.Value.Trim() != "")
                objFrm.Amount = double.Parse(txtAmount.Value);
            else
                objFrm.Amount = 0;

            objFrm.AmountWords = txtAmtWords.Value;
            if (txtCommenceDate.Text.Trim() != "")
                objFrm.CommenceDate = CommonClass.ConvertToSQLDate(txtCommenceDate.Text);
            else
                objFrm.CommenceDate = "";

            if (chkSavePDF.Checked)
                objFrm.SaveAsPDF = true;
            else
                objFrm.SaveAsPDF = false;

            if (objFrm.IsExistFormB())
            {
                if (UserType == "0")
                    objFrm.ModifiedBy = UID.ToString();
                else
                    objFrm.ModifiedBy = "";

                objFrm.UpdateFormB();
            }
            else
                objFrm.InsertFormB();

            objFrm = null;

            //Update PayeeRefno
            CommonClass objCommon = new CommonClass();
            objCommon.UserID = UserID;
            objCommon.UpdatePayeeRefNo();
            objCommon = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
}
