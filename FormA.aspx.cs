using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class FormA : System.Web.UI.Page
{
    int UserID = 0, UID = 0;
    string UserType = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteValidation.ValidateSession();
        SiteValidation.ValidateSSL(Request.Url.AbsoluteUri);
        UserType = Session["UserType"].ToString();
        UserID = int.Parse(Session["UserID"].ToString());
        UID = int.Parse(Session["UID"].ToString());
        txtDate.Attributes.Add("onkeypress", "return false");
        
        DateTime dt = DateTime.Now;
        txtDate.Value = dt.Day + "/" + dt.Month + "/" + dt.Year;

        if (!Page.IsPostBack)
        {
            GetApplicantName();
            FormAClass objA = new FormAClass();
            objA.UserID = UserID;
            if (objA.IsExistFormA())
                BindData();
            else
                GetPreFill();
            objA = null;
        }
        lblMsg.Text = "";
        
        if (Session["UserType"].ToString() == "0")
            lnkBak.Visible = true;
        else
            lnkBak.Visible = false;

        lblUnionNo.Text = Session["UnionNo"].ToString();
        txtEmployer.Focus();

        //Response.Expires = -1;
        //Response.AddHeader("pragma", "no-cache");
        //Response.CacheControl = "no-cache"; 
    }
    private void GetApplicantName()
    {
        try
        {
            DeclarationClass objDec = new DeclarationClass();
            objDec.UserID = UserID;
            hidFullname.Value = objDec.GetApplicantName();
            objDec = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }

    }
    private void GetPreFill()
    {
        try
        {
            FormAClass objFrm = new FormAClass();
            objFrm.UserID = UserID;
            DataTable dtFrm = objFrm.GetPreFillFormA();

            if (dtFrm.Rows.Count > 0)
            {
                txtEmployer.Value = dtFrm.Rows[0]["Employer"].ToString();
                txtSurname.Value = dtFrm.Rows[0]["Surname"].ToString();
                txtFirstname.Value = dtFrm.Rows[0]["Firstname"].ToString();
                txtGrade.Value = dtFrm.Rows[0]["Jobtitle"].ToString();
                txtEmployeeNo.Value = dtFrm.Rows[0]["EmployeeNo"].ToString();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
    private void BindData()
    {
        try
        {
            FormAClass objFrm = new FormAClass();
            objFrm.UserID = UserID;
            DataTable dtFrm = objFrm.GetFormA();

            if (dtFrm.Rows.Count > 0)
            {
                txtEmployer.Value = dtFrm.Rows[0]["EmployerName"].ToString();
                txtSurname.Value = dtFrm.Rows[0]["SurnameFormB"].ToString();
                txtFirstname.Value = dtFrm.Rows[0]["FirstnameFormB"].ToString();
                txtGrade.Value = dtFrm.Rows[0]["Grade"].ToString();
                txtEmployeeNo.Value = dtFrm.Rows[0]["EmpNoFB"].ToString();
                txtEmpPayGroup.Value = dtFrm.Rows[0]["EmpPayGroup"].ToString();
            }

            DeclarationClass objDec = new DeclarationClass();
            objDec.UserID = UserID;
            DataTable dtDec = objDec.GetDeclaration();

            if (dtDec.Rows.Count > 0)
            {
                txtSigned.Value = dtDec.Rows[0]["Signed"].ToString();
                txtDate.Value = dtDec.Rows[0]["DateDeclaration"].ToString();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("Success.aspx", false);
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("Declaration.aspx");
    }

    private void SaveData()
    {
        try
        {
            FormAClass objFrm = new FormAClass();
            objFrm.UserID = UserID;
            objFrm.EmployerName = txtEmployer.Value;
            objFrm.SurnameFormB = txtSurname.Value;
            objFrm.FirstnameFormB = txtFirstname.Value;
            objFrm.Grade = txtGrade.Value;
            objFrm.EmpNoFB = txtEmployeeNo.Value;
            objFrm.EmpPayGroup = txtEmpPayGroup.Value;

            if (objFrm.IsExistFormA())
            {
                if (UserType == "0")
                    objFrm.ModifiedBy = UID.ToString();
                else
                    objFrm.ModifiedBy = "";

                objFrm.UpdateFormA();
            }
            else
                objFrm.InsertFormA();

            objFrm = null;

            DeclarationClass objDec = new DeclarationClass();
            objDec.UserID = UserID;
            objDec.Signed = txtSigned.Value;
            objDec.DateDeclaration = CommonClass.ConvertToSQLDate(txtDate.Value);
            if (objDec.IsExistDeclaration())
            {
                if (UserType == "0")
                    objDec.ModifiedBy = UID.ToString();
                else
                    objDec.ModifiedBy = "";

                objDec.UpdateDeclarationFormA();
            }
            objDec = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
}
