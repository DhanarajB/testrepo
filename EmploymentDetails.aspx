<%@ Page Language="C#" MasterPageFile="~/MasterMain.master" AutoEventWireup="true"
    CodeFile="EmploymentDetails.aspx.cs" Inherits="EmploymentDetails" Title="Impact Membership Application" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    
    function ShowImpactDesc()
    {
        document.getElementById("<%=divImpactMember.ClientID%>").style.display="inline";
    }
    function HideImpactDesc()
    {
        document.getElementById('<%=txtImpactMemDetails.ClientID%>').value="";
        document.getElementById("<%=divImpactMember.ClientID%>").style.display="none";
    }
    
    function ShowAnotherDesc()
    {
        document.getElementById("<%=divAnotherMember.ClientID%>").style.display="inline";
    }
    function HideAnotherDesc()
    {
        document.getElementById('<%=txtAnotherMemDetails.ClientID%>').value="";
        document.getElementById("<%=divAnotherMember.ClientID%>").style.display="none";
    }
    
    function FormValidation()
    {
        var i=0;
        var ErrMsg = '';
        var CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>'
        
        if(document.getElementById('<%=txtEmployer.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Employer"</td></tr>';
            CommonMsg +='<tr><td>Employer</td></tr>';
        }
        
        if(document.getElementById('<%=txtWorkAdd1.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Workplace Address1"</td></tr>';
            CommonMsg +='<tr><td>Workplace Address1</td></tr>';
        }
        
        if(document.getElementById('<%=txtWorkAdd2.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Workplace Address2"</td></tr>';
            CommonMsg +='<tr><td>Workplace Address2</td></tr>';
        }
        
        if(document.getElementById('<%=txtJobTitle.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Grade/Job Title"</td></tr>';
            CommonMsg +='<tr><td>Grade/Job Title</td></tr>';
        }
        
        if(!document.getElementById('<%=rdYesImpactMember.ClientID%>').checked && !document.getElementById('<%=rdNoImpactMember.ClientID%>').checked)
        {
            i++;
            ErrMsg += '<tr><td>Please select the "IMPACT Membership"</td></tr>';
            CommonMsg +='<tr><td>IMPACT Membership</td></tr>';
        }
        
        if(!document.getElementById('<%=rdYesAnotherMem.ClientID%>').checked && !document.getElementById('<%=rdNoAnotherMem.ClientID%>').checked)
        {
            i++;
            ErrMsg += '<tr><td>Please select the "Membership of Another Union"</td></tr>';
            CommonMsg +='<tr><td>Membership of Another Union</td></tr>';
        }
        
        if(document.getElementById('<%=rdYesImpactMember.ClientID%>').checked && document.getElementById('<%=txtImpactMemDetails.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please give the details for "IMPACT Membership"</td></tr>';
            CommonMsg +='<tr><td>Details for IMPACT Membership</td></tr>';
        }    
        
        if(document.getElementById('<%=rdYesAnotherMem.ClientID%>').checked && document.getElementById('<%=txtAnotherMemDetails.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please give the details for "Membership of Another Union"</td></tr>';
            CommonMsg +='<tr><td>Details for Membership of Another Union</td></tr>';
        }
        if(!IsAmount(document.getElementById('<%=txtAnnualSalary.ClientID%>').value))
        {
            i++;
            ErrMsg += '<tr><td>Please enter the valid "Annual Salary"</td></tr>';                 
            CommonMsg +='<tr><td>Invalid Annual Salary</td></tr>';
        }

        if (ErrMsg != '')
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        return true;
    }
    
   
    var prefix=""
    var wd
    function NumberFormat(thisone)
    {
       // if (thisone.value.charAt(0)=="$")
       //     return
        wd="w"
        var tempnum=thisone.value
        for (i=0;i<tempnum.length;i++)
        {
            if (tempnum.charAt(i)==".")
            {
                wd="d"
                break;
            }
        }
    
        if (wd=="w")
            thisone.value=prefix+tempnum+".00"
        else
        {
            if (tempnum.charAt(tempnum.length-2)==".")
            {
                thisone.value=prefix+tempnum+"0"
            }
            else
            {
                //tempnum=Math.round(tempnum*100)/100
                //thisone.value=prefix+tempnum
            }
        }
    }
        
    function CheckIsNumber(e)
    {
        if(!e) e=window.event;
        keyval = e.keycode ? e.keycode : e.which;
         
        var keyval=event.keyCode;
    
        //alert(keyval);
        if(keyval==9 || keyval==37 || keyval==39 || keyval==8 ||keyval==190 || keyval==110 || keyval==46 || (keyval>=48 && keyval <=57) || (keyval>=96 && keyval<=105))
        {
           return true;
        }
        else
        {
          return false;
        }
    }
    
    function CheckPreviousValidation()
    {
        var ErrMsg = '';
        if(!IsAmount(document.getElementById('<%=txtAnnualSalary.ClientID%>').value))
        {
            ErrMsg += '<tr><td>Please enter the valid "Annual Salary"</td></tr>';                 
        }
    
        if (ErrMsg != '')
        {
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        return true;
    }
    
    function CheckNaN(ctrl)
    {
        if(IsAmount(document.getElementById('<%=txtAnnualSalary.ClientID%>').value))
        {
            //Convert Number Format  
            NumberFormat(ctrl);
            if(document.getElementById('<%=txtAnnualSalary.ClientID%>').value=='NaN')
            {
                document.getElementById('<%=txtAnnualSalary.ClientID%>').value='';
            }
            if(document.getElementById('<%=txtAnnualSalary.ClientID%>').value=='.00' || document.getElementById('<%=txtAnnualSalary.ClientID%>').value=='0.00' || document.getElementById('<%=txtAnnualSalary.ClientID%>').value=='0')
            {
                document.getElementById('<%=txtAnnualSalary.ClientID%>').value='';
            }
        }
    }
    
</script>

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="background-color: #de8518; border-bottom: 4px solid #9a5808;">
                            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 6px; padding-top: 6px;
                                float: left;">
                                <img src="images/Heading_bullet.jpg" alt="Impact" /></div>
                            <div class="PageHeading">Employment Details & Union Membership History</div><div class="back_to_list" id="lnkBak" runat="server">&laquo;&nbsp;<a href="UsersList.aspx" >Back to List</a> </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #de8518;
                    border-right: 1px solid #de8518; border-bottom: 1px solid #de8518">
                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                    <td class="content" colspan="4" align="right">Union No</td>
                                    <td align="left"><asp:Label ID="lblUnionNo" runat="server" Text="" CssClass="UnionCss"></asp:Label> </td>
                                </tr>
                                <tr>
                                    <td width="2%">
                                        &nbsp;</td>
                                    <td width="25%" align="right" class="content">
                                        Employer<span class="mandatory">*</span></td>
                                    <td align="left">
                                        <label>
                                            <input name="txtEmployer" type="text" class="textbox" id="txtEmployer" runat="server" tabindex="1" />
                                        </label>
                                    </td>
                                    <td width="26%" align="right" class="content">
                                        Grade/Job Title<span class="mandatory">*</span></td>
                                    <td width="24%" colspan="2" align="left">
                                        <label>
                                            <input name="txtJobTitle" type="text" class="textbox" id="txtJobTitle" runat="server" tabindex="9" />
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td align="right" class="content">
                                        Workplace Address 1<span class="mandatory">*</span></td>
                                    <td align="left">
                                        <label>
                                            <input name="txtWorkAdd1" type="text" class="textbox" id="txtWorkAdd1" runat="server" tabindex="2" />
                                        </label>
                                    </td>
                                    <td align="right" class="content">
                                        Employee/Personnel/<br />
                                        Staff Pay No</td>
                                    <td colspan="2" align="left">
                                        <label>
                                            <input name="txtEmployeeNo" type="text" class="textbox" id="txtEmployeeNo" runat="server" tabindex="10" />
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td align="right" class="content">
                                        2<span class="mandatory">*</span></td>
                                    <td align="left">
                                        <label>
                                            <input name="txtWorkAdd2" type="text" class="textbox" id="txtWorkAdd2" runat="server" tabindex="3" />
                                        </label>
                                    </td>
                                    <td align="right" class="content">
                                        Annual Salary&nbsp;&euro;<br />
                                        <span class="small_txt">(Only required in cases of Standing Order)</span></td>
                                    <td colspan="2" align="left">
                                        <label>
                                            <input name="txtAnnualSalary" type="text" class="textbox" id="txtAnnualSalary" runat="server" style="text-align:right" tabindex="11" onblur="return CheckNaN(this)" onkeydown="return CheckIsNumber(this);" />
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td align="right" class="content">
                                        3</td>
                                    <td align="left">
                                        <label>
                                            <input name="txtWorkAdd3" type="text" class="textbox" id="txtWorkAdd3" runat="server" tabindex="4" />
                                        </label>
                                    </td>
                                    <td colspan="3" align="center" class="h1">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="35">
                                        &nbsp;</td>
                                    <td align="right" class="content">
                                        4</td>
                                    <td align="left">
                                        <input name="txtWorkAdd4" type="text" class="textbox" id="txtWorkAdd4" runat="server" tabindex="5" /></td>
                                    <td align="right" class="content">
                                        &nbsp;</td>
                                    <td colspan="2" align="left">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="35">
                                    </td>
                                    <td align="right" class="content">
                                        5</td>
                                    <td align="left">
                                        <input name="txtWorkAdd4" type="text" class="textbox" id="txtWorkAdd5" runat="server" tabindex="5" /></td>
                                    <td align="right" class="content">
                                    </td>
                                    <td align="left" colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="35">
                                        &nbsp;</td>
                                    <td colspan="2" align="left" class="h1">
                                        IMPACT Membership</td>
                                    <td colspan="3" align="left" class="h1">
                                        Membership of Another Union</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td align="right" class="content">
                                        Have you been a member of IMPACT in the past?<span class="mandatory">*</span></td>
                                    <td align="left">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="14%">
                                                    <label>
                                                        <asp:RadioButton ID="rdYesImpactMember" runat="server" Text="Yes" CssClass="content" GroupName="ImpactMember" TabIndex="6" />
                                                    </label>
                                                </td>
                                                <td width="23%" class="content">
                                                    </td>
                                                <td width="10%">
                                                    <label>
                                                        <asp:RadioButton ID="rdNoImpactMember" runat="server" Text="No" CssClass="content" GroupName="ImpactMember" TabIndex="7"/>
                                                    </label>
                                                </td>
                                                <td width="53%" class="content">
                                                    </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" class="content">
                                        Are you or have you been a member of another trade union?<span class="mandatory">*</span></td>
                                    <td colspan="2" align="left">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="14%">
                                                    <label>
                                                        <asp:RadioButton ID="rdYesAnotherMem" runat="server" Text="Yes" CssClass="content" GroupName="AnotherMember" TabIndex="12" />
                                                    </label>
                                                </td>
                                                <td width="23%" class="content">
                                                    </td>
                                                <td width="10%">
                                                    <label>
                                                        <asp:RadioButton ID="rdNoAnotherMem" runat="server" Text="No" CssClass="content" GroupName="AnotherMember" TabIndex="13" />
                                                    </label>
                                                </td>
                                                <td width="53%" class="content">
                                                    </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td colspan="2" align="left" class="content">
                                        <div style="display: none;" id="divImpactMember" runat="server">
                                            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                                <tr>
                                                    <td align="right" valign="top">
                                                        If Yes, Please give details</td>
                                                    <td>
                                                        <label>
                                                            <textarea name="txtImpactMemDetails" cols="45" rows="3" class="textarea" id="txtImpactMemDetails" runat="server" tabindex="8"></textarea>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td colspan="3" align="right" class="content">
                                        <div style="display: none;" id="divAnotherMember" runat="server">
                                            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                                <tr>
                                                    <td width="51%" align="right" valign="top">
                                                        If Yes, Please give details</td>
                                                    <td width="49%" align="left">
                                                        <label>
                                                            <textarea name="txtAnotherMemDetails" cols="45" rows="5" class="textarea" id="txtAnotherMemDetails" runat="server" tabindex="14"></textarea>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                               <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="ErrorMsg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td align="left">
                                        &nbsp;</td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td colspan="2" align="left">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td align="left">
                                        &nbsp;</td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td align="right">
                                         <asp:Button ID="btnPrevious" runat="server" CssClass="button" Text="&laquo;&nbsp;Previous" OnClick="btnPrevious_Click" TabIndex="15"  OnClientClick="return CheckPreviousValidation()"/>
                                    </td>
                                    <td align="left" style="padding-right: 15px;">
                                         <asp:Button ID="btnNext" runat="server" CssClass="button" Text="Next&nbsp;&raquo;" OnClick="btnNext_Click" OnClientClick="return FormValidation()" TabIndex="16"/>
                                     </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
