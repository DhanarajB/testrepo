<%@ Page Language="C#" MasterPageFile="~/MasterMain.master" AutoEventWireup="true"
    CodeFile="Declaration.aspx.cs" Inherits="Declaration" Title="Impact Membership Application" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">

    function FormValidation()
    {
        var i=0;
        var ErrMsg = '';
        var CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>'
        
        if(document.getElementById('<%=txtFavouriteQuestion.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Favourite Question"</td></tr>';
            CommonMsg +='<tr><td>Favourite Question</td></tr>';
        }    
            
        
        if(document.getElementById('<%=txtFavouriteAnswer.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Favourite Answer"</td></tr>';
            CommonMsg +='<tr><td>Favourite Answer</td></tr>';
        }
            
        if(!document.getElementById('<%=rdYesDeduction.ClientID%>').checked && !document.getElementById('<%=rdNoDeduction.ClientID%>').checked)
        {
            i++;
            ErrMsg += '<tr><td>Please select the "Deduction at Source"</td></tr>';
            CommonMsg +='<tr><td>Deduction at Source</td></tr>';
        }

        if(!document.getElementById('<%=chkAgree.ClientID%>').checked)
        {
            i++;
            ErrMsg += '<tr><td>Please check the "IMPACT Membership Rules & Decisions" check box</td></tr>'
            CommonMsg +='<tr><td>IMPACT Membership Rules & Decisions check box</td></tr>';
        }
        
        /*if(document.getElementById('<%=txtSigned.ClientID%>').value!=document.getElementById('<%=txtApplicantName.ClientID%>').value)
        {
            i++;
            ErrMsg += '<tr><td>Please accept the terms and conditions check box</td></tr>';
            CommonMsg +='<tr><td>Acceptance of the terms and conditions check box</td></tr>';
        }*/
        
        if (ErrMsg != '')
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        
        var obj=document.getElementById('<%=btnNext.ClientID%>');
      __doPostBack(obj.name,'');
    }
    
    function SubmitForm()
    {
        if(document.getElementById('<%=txtFavouriteQuestion.ClientID%>').value=="")
            return false;
 
        if(document.getElementById('<%=txtFavouriteAnswer.ClientID%>').value=="")
            return false;
            
        if(!document.getElementById('<%=rdYesDeduction.ClientID%>').checked && !document.getElementById('<%=rdNoDeduction.ClientID%>').checked)
            return false;

        if(document.getElementById('<%=txtSigned.ClientID%>').value!=document.getElementById('<%=txtApplicantName.ClientID%>').value)
            return false;   
            
        if(!document.getElementById('<%=chkAgree.ClientID%>').checked)
            return false;
        
        var obj=document.getElementById('<%=btnNext.ClientID%>');
      __doPostBack(obj.name,'');
    }
    
    function ShowGuideline()
    {
       Guideline("<%=GuidelineText%>");
    }
    function SignProcess()
    {
        if(document.getElementById('<%=chkSign.ClientID%>').checked)
            document.getElementById('<%=txtSigned.ClientID%>').value = document.getElementById('<%=txtApplicantName.ClientID%>').value;
        else
            document.getElementById('<%=txtSigned.ClientID%>').value = '';
    }
</script>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="background-color: #de8518; border-bottom: 4px solid #9a5808;">
                            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 6px; padding-top: 6px;
                                float: left;">
                                <img src="images/Heading_bullet.jpg" alt="Impact" /></div>
                            <div class="PageHeading">Declaration</div><div class="back_to_list" id="lnkBak" runat="server">&laquo;&nbsp;<a href="UsersList.aspx">Back to List</a> </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #de8518;
                    border-right: 1px solid #de8518; border-bottom: 1px solid #de8518">
                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="2" style="padding-top:5px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="content" align="right" style="width:85%">Union No</td>
                                                <td>&nbsp;&nbsp;</td>
                                                <td align="left" style="width:15%"><asp:Label ID="lblUnionNo" runat="server" Text="" CssClass="UnionCss"></asp:Label> </td>            
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" valign="top" style="padding-left:30px; padding-right:30px; padding-bottom:5px; padding-top:5px;">
                                        <div class="Instruction_div">
                                            This security question is to verify your identity if you contact IMPACT by phone or email
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="50%" align="right" valign="top" class="content" style="padding-right: 10px;">
                                                    <br />
                                                    <br />
                                                    What is your favourite<span class="mandatory">*</span></td>
                                                <td width="50%" align="left" valign="top">
                                                    <br />
                                                    <br />
                                                    <input name="txtFavouriteQuestion" type="text" class="textbox" id="txtFavouriteQuestion" runat="server" tabindex="1"/>
                                                    &nbsp;<strong>?</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center" valign="top" class="small_txt">
                                                    (Write an item of your choice e.g. colour, holiday, destination, number, pet, car)<br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content" style="padding-right: 10px;">
                                                    Answer<span class="mandatory">*</span></td>
                                                <td align="left" valign="top">
                                                    <input name="txtFavouriteAnswer" type="text" class="textbox" id="txtFavouriteAnswer" runat="server" tabindex="2" />
                                                    <br />
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="7%" align="right" valign="top">
                                        <label>
                                            <input name="chkAgree" type="checkbox" id="chkAgree" tabindex="3" runat="server" />
                                        </label>
                                    </td>
                                    <td width="93%" align="left" valign="top" class="declaration_txt" style="padding-left: 15px;
                                        padding-right: 30px;"><span class="mandatory" style="font-size:12px; font-weight:bold">*</span>
                                        I hereby apply for membership of the Irish Municipal, Public and Civil Trade Union.
                                        I undertake to abide by the Union rules and decisions taken in accordance with these
                                        rules. I confirm that the information provided above is correct to the best of my
                                        knowledge.
                                        I acknowledge that my entitlement to assistance from the Union arises only from the date of joining the Union and only in respect of issues arising on or after that date.
                                        </td>
                                </tr>
                                <tr style="display:none">
                                    <td width="7%" align="right" valign="top">
                                        <label>
                                            <input name="chkSign" type="checkbox" id="chkSign" tabindex="3" runat="server" />
                                        </label>
                                    </td>
                                    <td width="93%" align="left" valign="top" class="declaration_txt" style="padding-left: 15px;
                                        padding-right: 30px;"><span class="mandatory" style="font-size:12px; font-weight:bold">*</span>
                                        Clicking this box indicates you�re acceptance of the terms and conditions of the IMPACT trade union
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top">
                                        <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                            <tr style="display:none">
                                                <td width="25%" align="right" valign="top" class="content">
                                                    Applicant Name</td>
                                                <td width="25%" align="left" valign="top">
                                                    <label>
                                                        <input name="txtApplicantName" type="text" class="textbox" id="txtApplicantName" runat="server" tabindex="4" />
                                                    </label>
                                                </td>
                                                <td width="45%" align="right" valign="top" class="content">
                                                    
                                                </td>
                                                <td width="5%" colspan="2" align="left" valign="top">
                                                    
                                                </td>
                                            </tr>
                                            <tr style="display:none">
                                                <td align="right" valign="top" class="content">
                                                    Signed</td>
                                                <td align="left" valign="top">
                                                    <label>
                                                        <input name="txtSigned" type="text" class="textbox" id="txtSigned" runat="server" tabindex="5" />
                                                    </label>
                                                </td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td colspan="2" align="left" valign="top">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr style="display:none">
                                                <td align="right" valign="top" class="content">
                                                    Date</td>
                                                <td align="left" valign="top">
                                                    <label>
                                                        <input name="txtDate" type="text" class="textbox" id="txtDate" runat="server" onkeypress="return false" tabindex="6" />
                                                    </label>
                                                </td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td colspan="2" align="left" valign="top">
                                                    &nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="h1" style="padding-left:30px">Payment Methods</td>
                                            </tr>
                                            <tr>
                                                 <td colspan="5"  align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr><td style="width:3%"></td>
                                                            <td style="width:94%" class="declartion_div">Payment of IMPACT subscriptions is through deduction at source from salary (where this facility is provided by the employer) or by standing order from the members bank account, or equivalent</td>
                                                            <td style="width:3%"></td>
                                                        </tr>
                                                    </table>
                                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width:3%"></td>
                                                            <td style="width:83%" class="content">Does your employer operate a deduction at source facility for IMPACT membership subscriptions?'<span class="mandatory">*</span></td>
                                                            <td style="width:7%" align="left">
                                                                <asp:RadioButton ID="rdYesDeduction" runat="server" CssClass="content" GroupName="Deduction" Text="Yes" TabIndex="7" />
                                                            </td>
                                                            <td style="width:7%" align="left">
                                                                <asp:RadioButton ID="rdNoDeduction" runat="server" CssClass="content" GroupName="Deduction" Text="No" TabIndex="8" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="ErrorMsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    &nbsp;</td>
                                                <td align="left" valign="top">
                                                    &nbsp;</td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td colspan="2" align="left" valign="top">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    &nbsp;</td>
                                                <td align="left" valign="top">
                                                    &nbsp;</td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td align="right" valign="top">
                                                    <asp:Button ID="btnPrevious" runat="server" Text="��Previous" CssClass="button" OnClick="btnPrevious_Click" TabIndex="9" />
                                                    </td>
                                                <td align="left" valign="top" style="display:none">
                                                     <asp:Button ID="btnNext" runat="server" Text="Next��" CssClass="button" OnClick="btnNext_Click" OnClientClick="return FormValidation()" TabIndex="10" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
