using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AdminDetail : System.Web.UI.Page
{
    int UserID = 0, UID = 0;
    string UserType = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteValidation.ValidateSession();
        SiteValidation.ValidateSSL(Request.Url.AbsoluteUri);
        UserType = Session["UserType"].ToString();
        UserID = int.Parse(Session["UserID"].ToString());
        UID = int.Parse(Session["UID"].ToString());

        lblMsg.Text = "";
        if (!Page.IsPostBack)
            BindData();
    }
    private void BindData()
    {
        try
        {
            AdminClass objAdmin = new AdminClass();
            objAdmin.UserID = UserID;
            DataTable dtAdm = objAdmin.GetAdminDetail();

            if (dtAdm.Rows.Count > 0)
            {
                txtCurSubRate.Value = dtAdm.Rows[0]["CurrentSubRate"].ToString();
                txtBasicSalary.Value = dtAdm.Rows[0]["BasicSalary"].ToString();

                txtPayeeRefNo.Value = dtAdm.Rows[0]["PayeeRefNo"].ToString();
                txtBranch.Value = dtAdm.Rows[0]["Branch"].ToString();
                txtMemberName.Value = dtAdm.Rows[0]["MemberName"].ToString();
                txtApprovedPhNo.Value = dtAdm.Rows[0]["PhoneNoApproval"].ToString();
                txtProcessedBy.Value = dtAdm.Rows[0]["ProcessedBy"].ToString();

                if (dtAdm.Rows[0]["IsApprovedByBranch"].ToString() == "True")
                    chkBranch.Checked = true;
                else if (dtAdm.Rows[0]["IsApprovedByBranch"].ToString() == "False")
                    chkBranch.Checked = false;

                txtApprovedDate.Value = dtAdm.Rows[0]["ApprovedDate"].ToString();
                txtSODate.Value = dtAdm.Rows[0]["SODate"].ToString();
                txtProcessDate.Value = dtAdm.Rows[0]["ProcessDate"].ToString();
            }
            dtAdm = null;
            objAdmin = null;

            //Bind Bank Details
            DataTable dtBank=CommonClass.GetBankDetails();
            if (dtBank.Rows.Count > 0)
            {
                lblImpAccName.Text = dtBank.Rows[0]["ImpAccountName"].ToString();
                lblBank.Text = dtBank.Rows[0]["Bank"].ToString();
                lblBankAddress.Text = dtBank.Rows[0]["BandAddress"].ToString();
                lblAccountNo.Text = dtBank.Rows[0]["Accountno"].ToString();
                lblSorCode.Text = dtBank.Rows[0]["SortCode"].ToString();
            }
            dtBank = null;

            //Get Status
            CommonClass objCommon = new CommonClass();
            objCommon.UserID = UserID;
            string status=objCommon.GetStatus();

            if (status == "Approved")
                chkStatus.Checked = true;
            else
                chkStatus.Checked = false;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveData();
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        SaveData();
        CommonClass objCommon = new CommonClass();
        objCommon.UserID = int.Parse(Session["UserID"].ToString());
        string form = objCommon.GetFormAFormB();
        if (form == "-1" || form == "")
        {
        }
        else if (form == "1")
        {
            Response.Redirect("FormA.aspx");
        }
        else if (form == "0")
        {
            Response.Redirect("FormB.aspx");
        }
        objCommon = null;
    }
    private void SaveData()
    {
        try
        {
            AdminClass objAdmin = new AdminClass();
            objAdmin.UserID = UserID;
            objAdmin.CurrentSubRate = txtCurSubRate.Value;
            
            if (txtBasicSalary.Value != "")
                objAdmin.BasicSalary = Convert.ToDecimal(txtBasicSalary.Value);
            else
                objAdmin.BasicSalary = 0;

            objAdmin.PayeeRefNo = txtPayeeRefNo.Value;
            objAdmin.Branch = txtBranch.Value;
            objAdmin.MemberName = txtMemberName.Value;
            
            if (txtApprovedDate.Value != "")
                objAdmin.ApprovedDate = CommonClass.ConvertToSQLDate(txtApprovedDate.Value);
            else
                objAdmin.ApprovedDate = "";

            objAdmin.PhoneNoApproval = txtApprovedPhNo.Value;

            if (chkBranch.Checked)
                objAdmin.IsApprovedByBranch = "1";
            else
                objAdmin.IsApprovedByBranch = "0";

            if (txtSODate.Value != "")
                objAdmin.SODate = CommonClass.ConvertToSQLDate(txtSODate.Value);
            else
                objAdmin.SODate = "";

            objAdmin.ProcessedBy = txtProcessedBy.Value;
            if (txtProcessDate.Value != "")
                objAdmin.ProcessDate = CommonClass.ConvertToSQLDate(txtProcessDate.Value);
            else
                objAdmin.ProcessDate = "";

            if (chkStatus.Checked)
                objAdmin.Status = "1";
            else
                objAdmin.Status = "0";

            if (objAdmin.IsExistAdminDetail())
            {
                if (UserType == "0")
                    objAdmin.ModifiedBy = UID.ToString();
                else
                    objAdmin.ModifiedBy = "";

                objAdmin.UpdateAdminDetail();
            }
            else
                objAdmin.InsertAdminDetail();

            objAdmin = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
}
