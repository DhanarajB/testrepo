using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Declaration : System.Web.UI.Page
{
    int UserID=0, UID=0;
    string UserType="1";
    public string GuidelineText = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteValidation.ValidateSession();
        SiteValidation.ValidateSSL(Request.Url.AbsoluteUri);
        UserType = Session["UserType"].ToString();
        UserID = int.Parse(Session["UserID"].ToString());
        UID = int.Parse(Session["UID"].ToString());
        
        DateTime dt = DateTime.Now;
        txtDate.Value = dt.Day + "/" + dt.Month + "/" + dt.Year;
        
        GetApplicantName();
        
        if(!Page.IsPostBack)
            BindData();

        rdYesDeduction.Attributes.Add("onclick", "return FormValidation();");
        rdNoDeduction.Attributes.Add("onclick", "return FormValidation();");
        txtApplicantName.Attributes.Add("onkeypress", "return false");
        chkSign.Attributes.Add("onclick", "SignProcess()");
        txtSigned.Attributes.Add("onkeypress", "return false");
        
        GetGuidelines();

        if (Session["UserType"].ToString() == "0")
            lnkBak.Visible = true;
        else
            lnkBak.Visible = false;

        lblUnionNo.Text = Session["UnionNo"].ToString();
        txtFavouriteQuestion.Focus();
    }
    private void GetGuidelines()
    {
        //GuidelineClass objGuide = new GuidelineClass();
        //DataTable dtGuid=objGuide.GetGuidelines();
        //if (dtGuid.Rows.Count > 0)
        //{
        //    GuidelineText = dtGuid.Rows[0]["Guidelines"].ToString();
        //}
    }
    private void GetApplicantName()
    {
        try
        {
            DeclarationClass objDec = new DeclarationClass();
            objDec.UserID = UserID;
            txtApplicantName.Value = objDec.GetApplicantName();
            objDec = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
        
    }
    private void BindData()
    {
        try
        {
            DeclarationClass objDec = new DeclarationClass();
            objDec.UserID = UserID;
            DataTable dtDec = objDec.GetDeclaration();

            if (dtDec.Rows.Count > 0)
            {
                txtFavouriteQuestion.Value = dtDec.Rows[0]["FavouriteQuestion"].ToString();
                txtFavouriteAnswer.Value = dtDec.Rows[0]["FavouriteAnswer"].ToString();
                txtSigned.Value = dtDec.Rows[0]["Signed"].ToString();
                txtDate.Value = dtDec.Rows[0]["DateDeclaration"].ToString();

                if (dtDec.Rows[0]["AcceptAgree"].ToString() == "True")
                    chkAgree.Checked = true;
                else
                    chkAgree.Checked = false;

                if (dtDec.Rows[0]["DeductionAtSource"].ToString() == "True")
                    rdYesDeduction.Checked = true;
                else if (dtDec.Rows[0]["DeductionAtSource"].ToString() == "False")
                    rdNoDeduction.Checked = true;

                if (dtDec.Rows[0]["SignAgree"].ToString() == "True")
                    chkSign.Checked = true;
                else
                    chkSign.Checked = false;

            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("EmploymentDetails.aspx");
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        SaveData();
        if (rdYesDeduction.Checked)
            Response.Redirect("FormA.aspx", false);
        else
            Response.Redirect("FormB.aspx", false);
    }
    private void SaveData()
    {
        try
        {
            DeclarationClass objDec = new DeclarationClass();
            objDec.UserID = UserID;

            objDec.FavouriteQuestion = txtFavouriteQuestion.Value;
            objDec.FavouriteAnswer = txtFavouriteAnswer.Value;
            objDec.ApplicantName = txtApplicantName.Value;
            objDec.Signed = txtSigned.Value;
            objDec.DateDeclaration = CommonClass.ConvertToSQLDate(txtDate.Value);

            if (chkAgree.Checked)
                objDec.AcceptAgree = "1";
            else
                objDec.AcceptAgree = "0";

            if (chkSign.Checked)
                objDec.SignAgree = "1";
            else
                objDec.SignAgree = "0";


            if (rdYesDeduction.Checked)
                objDec.DeductionAtSource = "1";
            else if(rdNoDeduction.Checked)
                objDec.DeductionAtSource = "0";

            if (objDec.IsExistDeclaration())
            {
                if (UserType == "0")
                    objDec.ModifiedBy = UID.ToString();
                else
                    objDec.ModifiedBy = "";

                objDec.UpdateDeclaration();
            }
            else
                objDec.InsertDeclaration();

            objDec = null;

            
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
}
