<%@ Page Language="C#" MasterPageFile="~/MasterMain.master" AutoEventWireup="true"
    CodeFile="FormA.aspx.cs" Inherits="FormA" Title="Impact Membership Application" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    function FormValidation()
    {
        var i=0;
        var ErrMsg = '';
        var CommonMsg ='';
        CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>'
        
        if(document.getElementById('<%=txtSurname.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "SurName"</td></tr>';
            CommonMsg +='<tr><td>SurName</td></tr>';
        }
        if(document.getElementById('<%=txtFirstname.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "FirstName(s)"</td></tr>';
            CommonMsg +='<tr><td>FirstName(s)</td></tr>';
        }
        if(document.getElementById('<%=txtGrade.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Grade"</td></tr>';
            CommonMsg +='<tr><td>Grade</td></tr>';
        }
        
        
        /*if(document.getElementById('<%=txtEmployeeNo.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Employee/Pay No"</td></tr>';
            CommonMsg +='<tr><td>Employee/Personal/Pay No</td></tr>';
        }*/
        
        if (ErrMsg != '')
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        
        return true;
    }
</script>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="background-color: #de8518; border-bottom: 4px solid #9a5808;">
                            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 6px; padding-top: 6px;
                                float: left;">
                                <img src="images/Heading_bullet.jpg" alt="Impact" /></div>
                            <div class="PageHeading">
                                AUTHORISATION FOR EMPLOYER TO DEDUCT UNION SUBSCRIPTIONS</div>
                            <div class="back_to_list" id="lnkBak" runat="server">
                                &laquo;&nbsp;<a href="UsersList.aspx">Back to List</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #de8518;
                    border-right: 1px solid #de8518; border-bottom: 1px solid #de8518">
                    <tr>
                        <td align="center" valign="top" style="padding: 10px;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="content" align="right" style="width:85%">Union No</td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td align="left" style="width:15%"><asp:Label ID="lblUnionNo" runat="server" Text="" CssClass="UnionCss"></asp:Label> </td>            
                                </tr>
                             </table>
                            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                    <td width="5%" ></td>
                                    <td width="90%" class="Instruction_div" align="left">At the end of the application process you will see an option to print a PDF version of this form that includes all your information. You should print that form, �Sign It� and send it back to Impact, Neary�s Court, Dublin 1 for transmission to your employer. <span style="color:red">Before you commence, please ensure that you have the facilities to either Save or Print the PDF version of this form on the computer terminal you are using.</span></td>
                                    <td width="5%" ></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-left: 30px;
                                padding-right: 30px;">
                                <tr>
                                    <td align="left" valign="top" class="pdf_h1" style="padding-bottom: 5px;">
                                        To</td>
                                     <td>
                                        
                                     </td>   
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="pdf_h1" colspan="2">
                                        Name of the Employer&nbsp;&nbsp;
                                        <input name="txtEmployer" type="text" class="textbox" id="txtEmployer" runat="server" tabindex="1" />
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="declaration_txt" colspan="2">
                                        Please deduct the IMPACT Union subscription, at the rate determined from time to
                                        time in accordance with the Rules of the Union, from my salary/wages and to pay
                                        this amount to IMPACT on my behalf. Please commence this deduction as soon as possible
                                        and continue it until further written or electronic notice either from me or IMPACT,
                                        as appropriate.<br />
                                        <br />
                                        I further request you to reinstate the deduction of my Union subscriptions to IMPACT
                                        following any period of career break or any other unpaid absence from work.<br />
                                        <br />
                                        I also authorise you to provide to IMPACT, in paper or electronic format, details
                                        of these deductions together with updates of the personal and employment related
                                        data set out in the IMPACT membership application form for use by it in connection
                                        with my Union membership.<br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" colspan="2">
                                        <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                            <tr>
                                                <td width="18%" align="right" valign="top" class="content">
                                                    SurName<span class="mandatory">*</span></td>
                                                <td width="28%" align="left" valign="top">
                                                    <input name="txtSurname" type="text" class="textbox" id="txtSurname" runat="server" tabindex="2" /></td>
                                                <td colspan="2" align="right" valign="top" class="content">
                                                    Employee/Personal/Pay No</td>
                                                <td width="22%" align="left" valign="top">
                                                    <input name="txtEmployeeNo" type="text" class="textbox" id="txtEmployeeNo" runat="server" tabindex="5" /></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    FirstName(s)<span class="mandatory">*</span></td>
                                                <td align="left" valign="top">
                                                    <input name="txtFirstname" type="text" class="textbox" id="txtFirstname" runat="server" tabindex="3" /></td>
                                                <td colspan="2" align="right" valign="top" class="content">
                                                    Employer's Pay group (if known)</td>
                                                <td align="left" valign="top">
                                                    <input name="txtEmpPayGroup" type="text" class="textbox" id="txtEmpPayGroup" runat="Server" tabindex="6" /></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    Grade<span class="mandatory">*</span></td>
                                                <td align="left" valign="top">
                                                    <input name="txtGrade" type="text" class="textbox" id="txtGrade" runat="server" tabindex="4" /></td>
                                                <td colspan="2" align="right" valign="top" class="content" style="display:none">
                                                    Signed<span class="mandatory">*</span></td>
                                                <td align="left" valign="top" style="display:none">
                                                    <input name="txtSigned" type="text" class="textbox" id="txtSigned" runat="Server" tabindex="7" /></td>
                                            </tr>
                                             <tr>
                                                <td align="right" valign="top" class="content">
                                                    </td>
                                                <td align="left" valign="top">
                                                    </td>
                                                <td colspan="2" align="right" valign="top" class="content" style="display:none">
                                                    Date&nbsp;</td>
                                                <td align="left" valign="top" style="display:none">
                                                    <input name="txtDate" type="text" class="textbox" id="txtDate" runat="Server" tabindex="8" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="ErrorMsg"></asp:Label>
                                                </td>
                                            </tr>
                                          </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <asp:HiddenField ID="hidFullname" runat="server" />
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right" valign="top" style="padding-right: 15px; padding-bottom: 10px;
                            padding-top: 10px;">
                            &nbsp;&nbsp;
                            <asp:Button id="btnPrevious" runat="server" Text="��Previous" CssClass="button" OnClick="btnPrevious_Click" TabIndex="9"  />
                            &nbsp;
                            <asp:Button id="btnNext" runat="server" Text="Save and Complete" CssClass="button" OnClick="btnNext_Click" TabIndex="10" OnClientClick="return  FormValidation();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
