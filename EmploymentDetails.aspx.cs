using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class EmploymentDetails : System.Web.UI.Page
{
    string UserType = "1";
    int UserID = 0, UnionNo=0;
    int UID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteValidation.ValidateSession();
        SiteValidation.ValidateSSL(Request.Url.AbsoluteUri);
        UserType = Session["UserType"].ToString();
        UserID = int.Parse(Session["UserID"].ToString());
        UID = int.Parse(Session["UID"].ToString());
        UnionNo = int.Parse(Session["UnionNo"].ToString());

        if (!Page.IsPostBack)
            BindData();

        rdYesImpactMember.Attributes.Add("onclick", "ShowImpactDesc()");
        rdNoImpactMember.Attributes.Add("onclick", "HideImpactDesc()");
        
        rdYesAnotherMem.Attributes.Add("onclick", "ShowAnotherDesc()");
        rdNoAnotherMem.Attributes.Add("onclick", "HideAnotherDesc()");

        if (Session["UserType"].ToString() == "0")
            lnkBak.Visible = true;
        else
            lnkBak.Visible = false;

        lblUnionNo.Text = Session["UnionNo"].ToString();
        txtEmployer.Focus();
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("PersonalDetails.aspx");
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        SaveData();
        Response.Redirect("Declaration.aspx");
    }

    private void SaveData()
    {
        try
        {
            EmpMemberClass objEmp = new EmpMemberClass();
            objEmp.UserID = UserID;

            objEmp.Employer = txtEmployer.Value;
            objEmp.WorkAddress1 = txtWorkAdd1.Value;
            objEmp.WorkAddress2 = txtWorkAdd2.Value;
            objEmp.WorkAddress3 = txtWorkAdd3.Value;
            objEmp.WorkAddress4 = txtWorkAdd4.Value;
            objEmp.WorkAddress5 = txtWorkAdd5.Value;
            objEmp.JobTitle = txtJobTitle.Value;
            objEmp.EmployeeNo = txtEmployeeNo.Value;

            if (txtAnnualSalary.Value.Trim() != "")
                objEmp.AnnualSalary = double.Parse(txtAnnualSalary.Value);
            else
                objEmp.AnnualSalary = 0;

            if (rdYesImpactMember.Checked)
                objEmp.IsImpactMember = "1";
            else if (rdNoImpactMember.Checked)
                objEmp.IsImpactMember = "0";

            objEmp.ImpactMemberDetail = txtImpactMemDetails.Value;

            if (rdYesAnotherMem.Checked)
                objEmp.IsAnotherUnionMember = "1";
            else if(rdNoAnotherMem.Checked)
                objEmp.IsAnotherUnionMember = "0";

            objEmp.AnotherUnionMemberDetail = txtAnotherMemDetails.Value;

            if (objEmp.IsExistEmpMem())
            {
                objEmp.UpdateEmpMemDetail();

                if (UserType == "0")
                    objEmp.ModifiedBy = UID.ToString();
                else
                    objEmp.ModifiedBy = "";
            }
            else
                objEmp.InsertEmpMemDetail();

            objEmp = null;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
    private void BindData()
    {
        try
        {
            EmpMemberClass objEmp = new EmpMemberClass();
            objEmp.UserID = UserID;
            DataTable dtEmp = objEmp.GetEmpMemDetail();
            
            if (dtEmp.Rows.Count > 0)
            {
                txtEmployer.Value = dtEmp.Rows[0]["Employer"].ToString();
                txtWorkAdd1.Value = dtEmp.Rows[0]["WorkAddress1"].ToString();
                txtWorkAdd2.Value = dtEmp.Rows[0]["WorkAddress2"].ToString();
                txtWorkAdd3.Value = dtEmp.Rows[0]["WorkAddress3"].ToString();
                txtWorkAdd4.Value = dtEmp.Rows[0]["WorkAddress4"].ToString();
                txtWorkAdd5.Value = dtEmp.Rows[0]["WorkAddress5"].ToString();
                txtJobTitle.Value = dtEmp.Rows[0]["JobTitle"].ToString();
                txtEmployeeNo.Value = dtEmp.Rows[0]["EmployeeNo"].ToString();

                if(dtEmp.Rows[0]["AnnualSalary"].ToString()!="0.00")
                    txtAnnualSalary.Value = dtEmp.Rows[0]["AnnualSalary"].ToString();

                if (dtEmp.Rows[0]["IsImpactMember"].ToString() == "True")
                {
                    rdYesImpactMember.Checked = true;
                    txtImpactMemDetails.Value = dtEmp.Rows[0]["ImpactMemberDetail"].ToString();
                    
                    divImpactMember.Style.Add("display", "inline");
                }
                else if (dtEmp.Rows[0]["IsImpactMember"].ToString() == "False")
                {
                    rdNoImpactMember.Checked = true;
                    txtImpactMemDetails.Value = "";
                    divImpactMember.Style.Add("display", "none");
                }

                if (dtEmp.Rows[0]["IsAnotherUnionMember"].ToString() == "True")
                {
                    rdYesAnotherMem.Checked = true;
                    txtAnotherMemDetails.Value = dtEmp.Rows[0]["AnotherUnionMemberDetail"].ToString();
                    divAnotherMember.Style.Add("display", "inline");
                }
                else if (dtEmp.Rows[0]["IsAnotherUnionMember"].ToString() == "False")
                {
                    rdNoAnotherMem.Checked = true;
                    txtAnotherMemDetails.Value = dtEmp.Rows[0]["AnotherUnionMemberDetail"].ToString();
                    divAnotherMember.Style.Add("display", "none");
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            ErrorLog objErr = new ErrorLog();
            objErr.WriteToLog(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, this.Page.GetType().Name);
            objErr = null;
        }
    }
}