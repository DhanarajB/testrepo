using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using
public partial class Email : System.Web.UI.Page
{
    int UserID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["Email"] != null && Session["Email"].ToString() != "")
            {
                txtEmail.Text = Session["Email"].ToString();
                txtcnfmEmail.Text = Session["Email"].ToString();
            }
        }

        UserID = int.Parse(Session["UserID"].ToString());
    }
    protected void Btnnext_Click(object sender, EventArgs e)
    {
        string emailid = txtEmail.Text;
        Session["Email"] = emailid;

        if (UserID == 0)
        {
            EmailClass email = new EmailClass();
            email.Email_Id = emailid;
            email.Status = "0";
            UserID = email.InsertEmail();
            Session["UserID"] = UserID;
            Session["UserType"] = "1";
            Session["UID"] = "0";

            CommonClass objCommon = new CommonClass();
            objCommon.UserID = UserID;
            Session["UnionNo"] = objCommon.GetNewUnionNumber().ToString();
            objCommon = null;
        }
        else
        {        
            EmailClass email = new EmailClass();
            email.Email_Id = emailid;
            email.Status = "0";
            email.UserID = int.Parse(Session["UserID"].ToString());
            email.UpdateEmail();
        }
        Response.Redirect("PersonalDetails.aspx");
    }
}   
