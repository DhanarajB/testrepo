using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;
/// <summary>
/// Summary description for FormAClass
/// </summary>
public class FormAClass
{
	public FormAClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _empname = "", _paid = "", _surname = "", _firstname = "", _grade = "", _empno = "", _paygroup = "", _modifiedby = "";
    int _userid = 0;

    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }
    public string EmployerName
    {
        get { return _empname; }
        set { _empname = value; }
    }
    public string Paid
    {
        get { return _paid; }
        set { _paid = value; }
    }
    public string SurnameFormB
    {
        get { return _surname; }
        set { _surname = value; }
    }
    public string FirstnameFormB
    {
        get { return _firstname; }
        set { _firstname = value; }
    }
    public string Grade
    {
        get { return _grade; }
        set { _grade = value; }
    }
    public string EmpNoFB
    {
        get { return _empno; }
        set { _empno = value; }
    }
    public string EmpPayGroup
    {
        get { return _paygroup; }
        set { _paygroup = value; }
    }
    public string ModifiedBy
    {
        get { return _modifiedby; }
        set { _modifiedby = value; }
    }

    public void InsertFormA()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertFormA";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@EmployerName", EmployerName);
        cmd.Parameters.AddWithValue("@Paid", Paid);
        cmd.Parameters.AddWithValue("@SurnameFormB", SurnameFormB);
        cmd.Parameters.AddWithValue("@FirstnameFormB", FirstnameFormB);
        cmd.Parameters.AddWithValue("@Grade", Grade);
        cmd.Parameters.AddWithValue("@EmpNoFB", EmpNoFB);
        cmd.Parameters.AddWithValue("@EmpPayGroup", EmpPayGroup);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void UpdateFormA()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateFormA";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@EmployerName", EmployerName);
        cmd.Parameters.AddWithValue("@Paid", Paid);
        cmd.Parameters.AddWithValue("@SurnameFormB", SurnameFormB);
        cmd.Parameters.AddWithValue("@FirstnameFormB", FirstnameFormB);
        cmd.Parameters.AddWithValue("@Grade", Grade);
        cmd.Parameters.AddWithValue("@EmpNoFB", EmpNoFB);
        cmd.Parameters.AddWithValue("@EmpPayGroup", EmpPayGroup);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public DataTable GetFormA()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetFormA";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public bool IsExistFormA()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_IsExistFormA";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        int id = int.Parse(dbf.ExecuteScalar(cmd));
        cmd.Dispose();

        bool res = false;
        if (id != 0)
            res = true;
        else
            res = false;

        return res;
    }

    public DataTable GetPreFillFormA()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetPreFillFormA";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

}
