using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;
/// <summary>
/// Summary description for AdminClass
/// </summary>
public class AdminClass
{
	public AdminClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DBFunctions dbf = new DBFunctions();
    int _userid = 0;
    string _currentsubrate = "", _payeerefno = "", _branch = "", _membername = "", _approveddate = "", _phonenoapproval = "", _isapprovbranch = "", _sodate = "", _processedby = "", _processdate = "", _modifiedby = "", _status="";
    decimal _basicsalary = 0;
    
    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }
    public string CurrentSubRate
    {
        get { return _currentsubrate; }
        set { _currentsubrate = value; }
    }
    public decimal BasicSalary
    {
        get { return _basicsalary; }
        set { _basicsalary = value; }
    }
    public string PayeeRefNo
    {
        get { return _payeerefno; }
        set { _payeerefno = value; }
    }
    public string Branch
    {
        get { return _branch; }
        set { _branch = value; }
    }
    public string MemberName
    {
        get { return _membername; }
        set { _membername = value; }
    }
    public string ApprovedDate
    {
        get { return _approveddate; }
        set { _approveddate = value; }
    }
    public string PhoneNoApproval
    {
        get { return _phonenoapproval; }
        set { _phonenoapproval = value; }
    }
    public string IsApprovedByBranch
    {
        get { return _isapprovbranch; }
        set { _isapprovbranch = value; }
    }
    public string SODate
    {
        get { return _sodate; }
        set { _sodate = value; }
    }
    public string ProcessedBy
    {
        get { return _processedby; }
        set { _processedby = value; }
    }
    public string ProcessDate
    {
        get { return _processdate; }
        set { _processdate = value; }
    }
    public string ModifiedBy
    {
        get { return _modifiedby; }
        set { _modifiedby = value; }
    }
    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    

    public void InsertAdminDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertAdminDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@CurrentSubRate", CurrentSubRate);
        cmd.Parameters.AddWithValue("@BasicSalary", BasicSalary);
        cmd.Parameters.AddWithValue("@PayeeRefNo", PayeeRefNo);
        cmd.Parameters.AddWithValue("@Branch", Branch);
        cmd.Parameters.AddWithValue("@MemberName", MemberName);

        if (ApprovedDate == null || ApprovedDate=="")
            cmd.Parameters.AddWithValue("@ApprovedDate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ApprovedDate", ApprovedDate);

        cmd.Parameters.AddWithValue("@PhoneNoApproval", PhoneNoApproval);
        cmd.Parameters.AddWithValue("@IsApprovedByBranch", IsApprovedByBranch);

        if (SODate == null || SODate=="")
            cmd.Parameters.AddWithValue("@SODate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@SODate", SODate);

        cmd.Parameters.AddWithValue("@ProcessedBy", ProcessedBy);

        if (ProcessDate == null || ProcessDate=="")
            cmd.Parameters.AddWithValue("@ProcessDate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ProcessDate", ProcessDate);

        cmd.Parameters.AddWithValue("@Status", Status);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void UpdateAdminDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateAdminDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@CurrentSubRate", CurrentSubRate);
        cmd.Parameters.AddWithValue("@BasicSalary", BasicSalary);
        cmd.Parameters.AddWithValue("@PayeeRefNo", PayeeRefNo);
        cmd.Parameters.AddWithValue("@Branch", Branch);
        cmd.Parameters.AddWithValue("@MemberName", MemberName);
        
        if (ApprovedDate == null || ApprovedDate == "")
            cmd.Parameters.AddWithValue("@ApprovedDate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ApprovedDate", ApprovedDate);

        cmd.Parameters.AddWithValue("@PhoneNoApproval", PhoneNoApproval);
        cmd.Parameters.AddWithValue("@IsApprovedByBranch", IsApprovedByBranch);
        
        if (SODate == null || SODate == "")
            cmd.Parameters.AddWithValue("@SODate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@SODate", SODate);

        cmd.Parameters.AddWithValue("@ProcessedBy", ProcessedBy);
        
        if (ProcessDate == null || ProcessDate == "")
            cmd.Parameters.AddWithValue("@ProcessDate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ProcessDate", ProcessDate);
        
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@Status", Status);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public DataTable GetAdminDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetAdminDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public bool IsExistAdminDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_IsExistAdminDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        int id = int.Parse(dbf.ExecuteScalar(cmd));
        cmd.Dispose();

        bool res = false;
        if (id != 0)
            res = true;
        else
            res = false;

        return res;
    }
}
