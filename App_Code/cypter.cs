using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
/// <summary>
/// Summary description for DEScypt
/// </summary>
public static class DEScypt
{
	
    const string DESKey = "AQWSEDRF";
    const string DESIV = "HGFEDCBA";


    //public static string DESDecrypt(string stringToDecrypt)//Decrypt the content
    //{

    //    byte[] key;
    //    byte[] IV;

    //    byte[] inputByteArray;
    //    try
    //    {

    //        key = Convert2ByteArray(DESKey);

    //        IV = Convert2ByteArray(DESIV);

    //        int len = stringToDecrypt.Length; inputByteArray = Convert.FromBase64String(stringToDecrypt);


    //        DESCryptoServiceProvider des = new DESCryptoServiceProvider();

    //        MemoryStream ms = new MemoryStream();

    //        CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
    //        cs.Write(inputByteArray, 0, inputByteArray.Length);

    //        cs.FlushFinalBlock();

    //        Encoding encoding = Encoding.UTF8;
    //        return encoding.GetString(ms.ToArray());
    //    }

    //    catch (System.Exception ex)
    //    {

    //        throw ex;
    //    }





    //}

    //public static string DESEncrypt(string stringToEncrypt)// Encrypt the content
    //{

    //    byte[] key;
    //    byte[] IV;

    //    byte[] inputByteArray;
    //    try
    //    {

    //        key = Convert2ByteArray(DESKey);

    //        IV = Convert2ByteArray(DESIV);

    //        inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
    //        DESCryptoServiceProvider des = new DESCryptoServiceProvider();

    //        MemoryStream ms = new MemoryStream(); CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
    //        cs.Write(inputByteArray, 0, inputByteArray.Length);

    //        cs.FlushFinalBlock();

    //        return Convert.ToBase64String(ms.ToArray());
    //    }

    //    catch (System.Exception ex)
    //    {

    //        throw ex;
    //    }

    //}

    //static byte[] Convert2ByteArray(string strInput)
    //{

    //    int intCounter; char[] arrChar;
    //    arrChar = strInput.ToCharArray();

    //    byte[] arrByte = new byte[arrChar.Length];

    //    for (intCounter = 0; intCounter <= arrByte.Length - 1; intCounter++)
    //        arrByte[intCounter] = Convert.ToByte(arrChar[intCounter]);

    //    return arrByte;
    //}
    public static string DESEncrypt(string Password)
    {
        string str = "&%#@?,:*";
        byte[] rgbKey = new byte[100];
        byte[] rgbIV = new byte[] { 0x12, 0x34, 0x56, 120, 0x90, 0xab, 0xcd, 0xef };
        try
        {
            rgbKey = Encoding.UTF8.GetBytes(str.Substring(0, 8));
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(Password);
            MemoryStream stream = new MemoryStream();
            CryptoStream stream2 = new CryptoStream(stream, provider.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
            stream2.Write(bytes, 0, bytes.Length);
            stream2.FlushFinalBlock();
            return Convert.ToBase64String(stream.ToArray());
        }
        catch (Exception exception)
        {
            return exception.Message;
        }
    }

    public static string DESDecrypt(string Password)
    {
        string str = "&%#@?,:*";
        byte[] rgbKey = new byte[100];
        byte[] rgbIV = new byte[] { 0x12, 0x34, 0x56, 120, 0x90, 0xab, 0xcd, 0xef };
        byte[] buffer = new byte[Password.Length];
        try
        {
            rgbKey = Encoding.UTF8.GetBytes(str.Substring(0, 8));
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            Password=Password.Replace(" ","+");
            buffer = Convert.FromBase64String(Password);
            MemoryStream stream = new MemoryStream();
            CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
            stream2.Write(buffer, 0, buffer.Length);
            stream2.FlushFinalBlock();
            return Encoding.UTF8.GetString(stream.ToArray());
        }
        catch (Exception exception)
        {
            return exception.Message;
        }
    }
}
