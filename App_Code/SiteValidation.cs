using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SiteValidation
/// </summary>
public class SiteValidation
{
	public SiteValidation()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void ValidateSession()
    {
        if (HttpContext.Current.Session["UserType"] == null || HttpContext.Current.Session["UserType"].ToString()=="")
            HttpContext.Current.Response.Redirect("SessionExpired.aspx");
        
        if (HttpContext.Current.Session["UserID"] == null || HttpContext.Current.Session["UserID"].ToString() == "")
            HttpContext.Current.Response.Redirect("SessionExpired.aspx");
    }
    public static void ValidateSSL(string URL)
    {
        //Local
        return;
        
        /* LIVE
        if (URL.IndexOf("http://") == -1)
            return;
        else
        {
            HttpContext.Current.Response.Redirect(URL.Replace("http://", "https://"));
        }
         */ 
    }
}
