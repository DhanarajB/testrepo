using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;

/// <summary>
/// Summary description for GuidelineClass
/// </summary>
public class GuidelineClass
{
	public GuidelineClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _guideline = "";
    
    public string Guideline
    {
        get { return _guideline; }
        set { _guideline = value; }
    }
    public DataTable GetGuidelines()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetGuidelines";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

}
