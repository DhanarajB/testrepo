using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;


/// <summary>
/// Summary description for DeclarationClass
/// </summary>
public class DeclarationClass
{
	public DeclarationClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _favouritequst = "", _favoans = "", _signed = "", _datedeclar = "", _deductionsource = "", _modifiedby = "", _appname = "", _acceptagree = "", _signagree = "";
    int _userid = 0;
    Boolean _payByStandingOrder;
    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }
    public string FavouriteQuestion
    {
        get { return _favouritequst; }
        set { _favouritequst = value; }
    }
    public string FavouriteAnswer
    {
        get { return _favoans; }
        set { _favoans = value; }
    }

    public string ApplicantName
    {
        get { return _appname; }
        set { _appname = value; }
    }

    public string Signed
    {
        get { return _signed; }
        set { _signed = value; }
    }
    public string DateDeclaration
    {
        get { return _datedeclar; }
        set { _datedeclar = value; }
    }
    public string DeductionAtSource
    {
        get { return _deductionsource; }
        set { _deductionsource = value; }
    }
    public string ModifiedBy
    {
        get { return _modifiedby; }
        set { _modifiedby = value; }
    }
    public string AcceptAgree
    {
        get { return _acceptagree; }
        set { _acceptagree = value; }
    }
    public string SignAgree
    {
        get { return _signagree; }
        set { _signagree = value; }
    }
    public Boolean payByStandingOrder
    {
        get { return _payByStandingOrder; }
        set { _payByStandingOrder = value; }
    }
    
    

    public void InsertDeclaration()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertDeclaration";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@FavouriteQuestion", FavouriteQuestion);
        cmd.Parameters.AddWithValue("@FavouriteAnswer", FavouriteAnswer);
        cmd.Parameters.AddWithValue("@Signed", Signed);

        if (DateDeclaration.ToString()=="" || DateDeclaration.ToString() == "1/1/0001 12:00:00 AM")
        {
            System.Data.SqlTypes.SqlDateTime sqldatenull = System.Data.SqlTypes.SqlDateTime.Null;
            cmd.Parameters.AddWithValue("@DateDeclaration", sqldatenull);
        }
        else
            cmd.Parameters.AddWithValue("@DateDeclaration", DateDeclaration);
        
        
             
        if (DeductionAtSource == null || DeductionAtSource=="")
            cmd.Parameters.AddWithValue("@DeductionAtSource", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@DeductionAtSource", DeductionAtSource);

        if (AcceptAgree == null || AcceptAgree == "")
            cmd.Parameters.AddWithValue("@AcceptAgree", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@AcceptAgree", AcceptAgree);

        if (SignAgree == null || SignAgree == "")
            cmd.Parameters.AddWithValue("@SignAgree", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@SignAgree", SignAgree);

        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void InsertDeclaration_NOC()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertDeclarationNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@FavouriteQuestion", FavouriteQuestion);
        cmd.Parameters.AddWithValue("@FavouriteAnswer", FavouriteAnswer);
        cmd.Parameters.AddWithValue("@Signed", Signed);
        cmd.Parameters.AddWithValue("@DateDeclaration", DateDeclaration);
        cmd.Parameters.AddWithValue("@payByStandingOrder", payByStandingOrder);
     
        if (DeductionAtSource == null || DeductionAtSource=="")
            cmd.Parameters.AddWithValue("@DeductionAtSource", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@DeductionAtSource", DeductionAtSource);

        cmd.Parameters.AddWithValue("@AcceptAgree", AcceptAgree);
        cmd.Parameters.AddWithValue("@SignAgree", SignAgree);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void InsertPayment_NOC()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertPaymentNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        
        if (DeductionAtSource == null || DeductionAtSource == "")
            cmd.Parameters.AddWithValue("@DeductionAtSource", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@DeductionAtSource", DeductionAtSource);

        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void InsertFavourite_NOC()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertFavouriteNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@FavouriteQuestion", FavouriteQuestion);
        cmd.Parameters.AddWithValue("@FavouriteAnswer", FavouriteAnswer);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }



    public void UpdateDeclaration()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateDeclaration";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@FavouriteQuestion", FavouriteQuestion);
        cmd.Parameters.AddWithValue("@FavouriteAnswer", FavouriteAnswer);
        cmd.Parameters.AddWithValue("@Signed", Signed);

        cmd.Parameters.AddWithValue("@DateDeclaration", DateDeclaration);
        
        if (DeductionAtSource == null || DeductionAtSource == "")
            cmd.Parameters.AddWithValue("@DeductionAtSource", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@DeductionAtSource", DeductionAtSource);

        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);

        if (AcceptAgree == null || AcceptAgree == "")
            cmd.Parameters.AddWithValue("@AcceptAgree", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@AcceptAgree", AcceptAgree);

        if (SignAgree == null || SignAgree == "")
            cmd.Parameters.AddWithValue("@SignAgree", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@SignAgree", SignAgree);

        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void UpdateDeclarationFormA()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateDeclarationFormA";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Signed", Signed);
        cmd.Parameters.AddWithValue("@DateDeclaration", DateDeclaration);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void UpdateDeclaration_NOC()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateDeclarationNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@FavouriteQuestion", FavouriteQuestion);
        cmd.Parameters.AddWithValue("@FavouriteAnswer", FavouriteAnswer);
        cmd.Parameters.AddWithValue("@Signed", Signed);
        cmd.Parameters.AddWithValue("@DateDeclaration", DateDeclaration);
        cmd.Parameters.AddWithValue("@payByStandingOrder", payByStandingOrder);
        if (DeductionAtSource == null || DeductionAtSource == "")
            cmd.Parameters.AddWithValue("@DeductionAtSource", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@DeductionAtSource", DeductionAtSource);

        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@AcceptAgree", AcceptAgree);
        cmd.Parameters.AddWithValue("@SignAgree", SignAgree);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void UpdatePayment_NOC()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdatePaymentNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);

        if (DeductionAtSource == null || DeductionAtSource == "")
            cmd.Parameters.AddWithValue("@DeductionAtSource", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@DeductionAtSource", DeductionAtSource);

        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void UpdateFavourite_NOC()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateFavourite_NOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@FavouriteQuestion", FavouriteQuestion);
        cmd.Parameters.AddWithValue("@FavouriteAnswer", FavouriteAnswer);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public DataTable GetDeclaration()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetDeclaration";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public bool IsExistDeclaration()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_IsExistDeclaration";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        int id = int.Parse(dbf.ExecuteScalar(cmd));
        cmd.Dispose();

        bool res = false;
        if (id != 0)
            res = true;
        else
            res = false;

        return res;
    }

    public string GetApplicantName()
    {
        string fullname = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetApplicantName";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        if (dt.Rows.Count > 0)
            fullname = dt.Rows[0]["Fullname"].ToString();
        
        return fullname;
    }
    public string GetFirstName()
    {
        string fullname = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetFirstName";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        if (dt.Rows.Count > 0)
            fullname = dt.Rows[0]["Firstname"].ToString();
        return fullname;
    }

    public string GetFormAFormB()
    {
        string IsFormA= "0";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetFormAFormB";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        IsFormA = dbf.ExecuteScalar(cmd);
        cmd.Dispose();
        return IsFormA;
    }
}
