using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DBLibrary;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Email
/// </summary>
public class EmailClass
{
    public EmailClass()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _EmailID,_status;
    int _userid;

    public string Email_Id
    {
        get
        {
            return _EmailID;
        }
        set
        {
            _EmailID = value;
        }
    }

    public string Status
    {
        get
        {
            return _status;
        }
        set
        {
            _status = value;
        }
    }

    public int UserID
    {
        get
        {
            return _userid;
        }
        set
        {
            _userid = value;
        }
    }


    public int InsertEmail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "Spl_InsertEmail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@emailid", Email_Id);
        cmd.Parameters.AddWithValue("@emailalready", Status);
        cmd.Parameters.Add("@error", SqlDbType.Char, 500);
        cmd.Parameters["@error"].Direction = ParameterDirection.Output;       
        dbf.ExecuteNonQuery(cmd);
        int val = Convert.ToInt32(cmd.Parameters["@error"].Value);        
        cmd.Dispose();
        return val;
    }

    public void UpdateEmail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "Spl_UpdateEmail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@emailid", Email_Id);
        cmd.Parameters.AddWithValue("@emailalready", Status);
        cmd.Parameters.AddWithValue("@userid", UserID); 
        dbf.ExecuteNonQuery(cmd);        
        cmd.Dispose();  
    }


}