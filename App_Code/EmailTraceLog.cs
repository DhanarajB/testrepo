using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for EmailTraceLog
/// </summary>
public class EmailTraceLog
{
	public EmailTraceLog()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void WriteToLog(string Ex, string FunctionName, string PageName)
    {
        string Message = Ex;
        string txtfilepath = HttpContext.Current.Server.MapPath("~/ErrorLog/EmailLog/");
        System.Text.StringBuilder ContentToWrite;
        System.IO.StreamWriter LogStreamWriter;
        System.IO.FileStream LogFileStream;
        string LogFile;
        LogFile = txtfilepath + System.DateTime.Now.ToString("yyyyMMMdd") + ".html";

        if (System.IO.File.Exists(LogFile) == false)
        {
            ContentToWrite = new System.Text.StringBuilder();
            LogFileStream = new System.IO.FileStream(LogFile, System.IO.FileMode.Append);
            LogStreamWriter = new System.IO.StreamWriter(LogFileStream, System.Text.Encoding.UTF8);
            ContentToWrite.Append("<html><head></head><body><h4>Impact Membership Application Email Log on " + DateTime.Now.ToString("dd-MMM-yyyy") + "</h4>");
            ContentToWrite.Append("<table border='1px' cellpadding='0' cellspacing'0' style='font-size:8pt; font-family:Arial; border-style:solid; border-collapse:collapse; border-color:black; width:100%'>");
            ContentToWrite.Append("<tr valign='top' style='font-size:12px'>");
            ContentToWrite.Append("<td width='10%'><b>Time</b></td>");
            ContentToWrite.Append("<td width='70%'><b>Message</b></td>");
            ContentToWrite.Append("<td width='10%'><b>Function</b></td>");
            ContentToWrite.Append("<td width='10%'><b>Page</b></td>");
            ContentToWrite.Append("</tr>");

            LogStreamWriter.Write(ContentToWrite.ToString());
            LogStreamWriter.Flush();
            LogStreamWriter.Close();
            LogFileStream.Close();
        }
        ContentToWrite = new System.Text.StringBuilder();
        LogFileStream = new System.IO.FileStream(LogFile, System.IO.FileMode.Append);
        LogStreamWriter = new System.IO.StreamWriter(LogFileStream, System.Text.Encoding.UTF8);
        ContentToWrite.Append("<tr valign='top'>");
        ContentToWrite.Append("<td>" + DateTime.Now.ToString("hh:mm:ss tt") + "</td>");
        ContentToWrite.Append("<td>" + Message + "<br>" + "</td>");
        ContentToWrite.Append("<td>" + FunctionName + "</td>");
        ContentToWrite.Append("<td>" + PageName + "</td>");
        ContentToWrite.Append("</tr>");
        LogStreamWriter.Write(ContentToWrite.ToString());
        LogStreamWriter.Flush();
        LogStreamWriter.Close();
        LogFileStream.Close();
    }
}
