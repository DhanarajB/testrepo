using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DBDirects
/// </summary>
public class DBDirects
{
	public DBDirects()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet MyQuery(string Query)
    {
        //DBFunctions.dbConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();        
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        SqlCommand cmd = new SqlCommand("spl_MyQuery", con);
        cmd.CommandType = CommandType.StoredProcedure;

        SqlDataAdapter da = new SqlDataAdapter();

        cmd.Parameters.AddWithValue("@Query", Query);

        da.SelectCommand = cmd;

        DataSet ds = new DataSet();
        da.Fill(ds, "MyQuery");

        return ds;

    }
}
