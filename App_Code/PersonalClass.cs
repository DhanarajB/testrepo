using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;

/// <summary>
/// Summary description for PersonalClass
/// </summary>
public class PersonalClass
{
	public PersonalClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _surname = "", _firstname = "", _surname1 = "", _firstname1 = "", _homeadd1 = "", _homeadd2 = "", _homeadd3 = "", _homeadd4 = "", _homeadd5 = "", _gender = "", _phoneno = "", _mobileno = "", _dob = "", _email = "", _ppsno = "", _modifiedby = "";
    string _Employer = "", _jobTitle = "", _staffNo = "", _prevHomeAdd1 = "", _prevHomeAdd2 = "", _prevHomeAdd3 = "", _prevHomeAdd4 = "";
    int _userid = 0;
    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }
    public string Surname
    {
        get { return _surname; }
        set { _surname = value; }
    }
    public string Firstname
    {
        get { return _firstname; }
        set { _firstname = value; }
    }
    public string Surname1
    {
        get { return _surname1; }
        set { _surname1 = value; }
    }
    public string Firstname1
    {
        get { return _firstname1; }
        set { _firstname1 = value; }
    }
    public string HomeAddress1
    {
        get { return _homeadd1; }
        set { _homeadd1 = value; }
    }
    public string HomeAddress2
    {
        get { return _homeadd2; }
        set { _homeadd2 = value; }
    }
    public string HomeAddress3
    {
        get { return _homeadd3; }
        set { _homeadd3 = value; }
    }
    public string HomeAddress4
    {
        get { return _homeadd4; }
        set { _homeadd4 = value; }
    }
    public string HomeAddress5
    {
        get { return _homeadd5; }
        set { _homeadd5 = value; }
    }
    
    public string Gender
    {
        get { return _gender; }
        set { _gender = value; }
    }

    public string PhoneNo
    {
        get { return _phoneno; }
        set { _phoneno = value; }
    }
    public string MobileNo
    {
        get { return _mobileno; }
        set { _mobileno = value; }
    }

    public string DOB
    {
        get { return _dob; }
        set { _dob = value; }
    }
    public string Email
    {
        get { return _email; }
        set { _email = value; }
    }
    public string PPSNo
    {
        get { return _ppsno; }
        set { _ppsno = value; }
    }
    public string ModifiedBy
    {
        get { return _modifiedby; }
        set { _modifiedby = value; }
    }
     public string Employer
    {
        get { return _Employer; }
        set { _Employer = value; }
    }
     public string jobTitle
    {
        get { return _jobTitle; }
        set { _jobTitle = value; }
    }
     public string staffNo
    {
        get { return _staffNo; }
        set { _staffNo = value; }
    }
     public string prevHomeAdd1
    {
        get { return _prevHomeAdd1; }
        set { _prevHomeAdd1 = value; }
    }
     public string prevHomeAdd2
        {
            get { return _prevHomeAdd2; }
            set { _prevHomeAdd2 = value; }
        }
     public string prevHomeAdd3
        {
            get { return _prevHomeAdd3; }
            set { _prevHomeAdd3 = value; }
        }
     public string prevHomeAdd4
        {
            get { return _prevHomeAdd4; }
            set { _prevHomeAdd4 = value; }
        }

    public int InsertPersonelDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertPersonelDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Surname", Surname);
        cmd.Parameters.AddWithValue("@Firstname", Firstname);
        cmd.Parameters.AddWithValue("@Surname1", Surname1);
        cmd.Parameters.AddWithValue("@Firstname1", Firstname1);
        cmd.Parameters.AddWithValue("@HomeAddress1", HomeAddress1);
        cmd.Parameters.AddWithValue("@HomeAddress2", HomeAddress2);
        cmd.Parameters.AddWithValue("@HomeAddress3", HomeAddress3);
        cmd.Parameters.AddWithValue("@HomeAddress4", HomeAddress4);
        cmd.Parameters.AddWithValue("@HomeAddress5", HomeAddress5);
        cmd.Parameters.AddWithValue("@Gender", Gender);
        cmd.Parameters.AddWithValue("@PhoneNo", PhoneNo);
        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
        cmd.Parameters.AddWithValue("@DOB", DOB);
        cmd.Parameters.AddWithValue("@Email", Email);
        cmd.Parameters.AddWithValue("@PPSNo", PPSNo);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        int id = int.Parse(dbf.ExecuteScalar(cmd));
        cmd.Dispose();
        return id;
    }

    public void UpdatePersonelDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdatePersonelDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Surname", Surname);
        cmd.Parameters.AddWithValue("@Firstname", Firstname);
        cmd.Parameters.AddWithValue("@Surname1", Surname1);
        cmd.Parameters.AddWithValue("@Firstname1", Firstname1);
        cmd.Parameters.AddWithValue("@HomeAddress1", HomeAddress1);
        cmd.Parameters.AddWithValue("@HomeAddress2", HomeAddress2);
        cmd.Parameters.AddWithValue("@HomeAddress3", HomeAddress3);
        cmd.Parameters.AddWithValue("@HomeAddress4", HomeAddress4);
        cmd.Parameters.AddWithValue("@HomeAddress5", HomeAddress5);
        cmd.Parameters.AddWithValue("@Gender", Gender);
        cmd.Parameters.AddWithValue("@PhoneNo", PhoneNo);
        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
        cmd.Parameters.AddWithValue("@DOB", DOB);
        cmd.Parameters.AddWithValue("@Email", Email);
        cmd.Parameters.AddWithValue("@PPSNo", PPSNo);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);

        dbf = new DBFunctions();
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public DataTable GetPersonalDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetPersonalDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public DataTable GetUserInfo()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetUserInfo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public DataTable GetPersonalUserInfo()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "sp_impact_getUserInfo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public DataTable GetChangeOfAddInfo()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "sp_impact_getChangeOfAddInfo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public DataTable GetChangeOfPersonalAddInfo()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "sp_impact_getChangeOfPersonalAddInfo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public DataTable GetChangeOfHomeAdd()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "sp_impact_getChangeOfHomeAdd";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    
    public DataTable GetChangeOfHomeAdd1()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "sp_impact_getChangeOfHomeAdd1";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }
    

    public void UpdateChangeOfHomeAddress()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateHomeAddress";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Surname", Surname);
        cmd.Parameters.AddWithValue("@Firstname", Firstname);
        cmd.Parameters.AddWithValue("@HomeAddress1", HomeAddress1);
        cmd.Parameters.AddWithValue("@HomeAddress2", HomeAddress2);
        cmd.Parameters.AddWithValue("@HomeAddress3", HomeAddress3);
        cmd.Parameters.AddWithValue("@HomeAddress4", HomeAddress4);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@employer", Employer);
        cmd.Parameters.AddWithValue("@jobTitle", jobTitle);
        cmd.Parameters.AddWithValue("@staffNo", staffNo);
        cmd.Parameters.AddWithValue("@prevHomeAdd1", prevHomeAdd1);
        cmd.Parameters.AddWithValue("@prevHomeAdd2", prevHomeAdd2);
        cmd.Parameters.AddWithValue("@prevHomeAdd3", prevHomeAdd3);
        cmd.Parameters.AddWithValue("@prevHomeAdd4", prevHomeAdd4);

        dbf = new DBFunctions();
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }


    public void UpdateNOCFlag()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "sp_impact_update_noc";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);

        dbf = new DBFunctions();
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
}