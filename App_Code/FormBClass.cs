using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;


/// <summary>
/// Summary description for FormBClass
/// </summary>
public class FormBClass
{
	public FormBClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _bankname = "", _accountno = "", _sortcode = "", _bankadd1 = "", _bankadd2 = "", _bankadd3 = "", _bankadd4 = "", _accname1 = "", _accname2 = "", _acctype = "", _amtwords = "", _commencedate = "", _modifiedby = "";
    int _userid = 0;
    double _amount = 0;
    bool _saveaspdf = false;
    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }
    public string BankName
    {
        get { return _bankname; }
        set { _bankname = value; }
    }
    public string AccountNo
    {
        get { return _accountno; }
        set { _accountno = value; }
    }
    public string SortCode
    {
        get { return _sortcode; }
        set { _sortcode = value; }
    }
    public string BankAddress1
    {
        get { return _bankadd1; }
        set { _bankadd1 = value; }
    }
    public string BankAddress2
    {
        get { return _bankadd2; }
        set { _bankadd2 = value; }
    }
    public string BankAddress3
    {
        get { return _bankadd3; }
        set { _bankadd3 = value; }
    }
    public string BankAddress4
    {
        get { return _bankadd4; }
        set { _bankadd4 = value; }
    }
    public string AccountName1
    {
        get { return _accname1; }
        set { _accname1 = value; }
    }
    public string AccountName2
    {
        get { return _accname2; }
        set { _accname2 = value; }
    }
    public string AccoutType
    {
        get { return _acctype; }
        set { _acctype = value; }
    }
    
    public double Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }
    public string AmountWords
    {
        get { return _amtwords; }
        set { _amtwords = value; }
    }
    public string CommenceDate
    {
        get { return _commencedate; }
        set { _commencedate = value; }
    }
    public bool SaveAsPDF
    {
        get { return _saveaspdf; }
        set { _saveaspdf = value; }
    }
    public string ModifiedBy
    {
        get { return _modifiedby; }
        set { _modifiedby = value; }
    }
    
    public void InsertFormB()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertFormB";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@BankName", BankName);
        cmd.Parameters.AddWithValue("@AccountNo", AccountNo);
        cmd.Parameters.AddWithValue("@SortCode", SortCode);
        cmd.Parameters.AddWithValue("@BankAddress1", BankAddress1);
        cmd.Parameters.AddWithValue("@BankAddress2", BankAddress2);
        cmd.Parameters.AddWithValue("@BankAddress3", BankAddress3);
        cmd.Parameters.AddWithValue("@BankAddress4", BankAddress4);
        cmd.Parameters.AddWithValue("@AccountName1", AccountName1);
        cmd.Parameters.AddWithValue("@AccountName2", AccountName2);
        cmd.Parameters.AddWithValue("@AccoutType", AccoutType);
        cmd.Parameters.AddWithValue("@Amount", Amount);
        cmd.Parameters.AddWithValue("@AmountWords", AmountWords);

        if(CommenceDate==null || CommenceDate=="")
            cmd.Parameters.AddWithValue("@CommenceDate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@CommenceDate", CommenceDate);

        cmd.Parameters.AddWithValue("@SaveAsPDF", SaveAsPDF);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void UpdateFormB()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateFormB";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@BankName", BankName);
        cmd.Parameters.AddWithValue("@AccountNo", AccountNo);
        cmd.Parameters.AddWithValue("@SortCode", SortCode);
        cmd.Parameters.AddWithValue("@BankAddress1", BankAddress1);
        cmd.Parameters.AddWithValue("@BankAddress2", BankAddress2);
        cmd.Parameters.AddWithValue("@BankAddress3", BankAddress3);
        cmd.Parameters.AddWithValue("@BankAddress4", BankAddress4);
        cmd.Parameters.AddWithValue("@AccountName1", AccountName1);
        cmd.Parameters.AddWithValue("@AccountName2", AccountName2);
        cmd.Parameters.AddWithValue("@AccoutType", AccoutType);
        cmd.Parameters.AddWithValue("@Amount", Amount);
        cmd.Parameters.AddWithValue("@AmountWords", AmountWords);

        if (CommenceDate == null || CommenceDate == "")
            cmd.Parameters.AddWithValue("@CommenceDate", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@CommenceDate", CommenceDate);

        cmd.Parameters.AddWithValue("@SaveAsPDF", SaveAsPDF);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public DataTable GetFormB()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetFormB";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public bool IsExistFormB()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_IsExistFormB";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        int id = int.Parse(dbf.ExecuteScalar(cmd));
        cmd.Dispose();

        bool res = false;
        if (id != 0)
            res = true;
        else
            res = false;

        return res;
    }

    public bool IsDownload()
    {
        bool res = false;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_IsDownload";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["SaveAsPDF"].ToString() == "True")
                res = true;
        }
        cmd.Dispose();

        return res;
    }
}
