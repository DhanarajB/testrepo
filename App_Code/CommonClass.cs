using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;


/// <summary>
/// Summary description for CommonClass
/// </summary>
public class CommonClass
{
	public CommonClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    int _userid = 0;

    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }

    public static string ConvertToSQLDate(string Date)
    {
        string frmDay = "", frmMon = "", frmYear = "";
        frmDay = Date.Substring(0, Date.IndexOf("/"));
        frmMon = Date.Substring(Date.IndexOf("/") + 1, Date.LastIndexOf("/") - Date.IndexOf("/") - 1);
        frmYear = Date.Substring(Date.LastIndexOf("/") + 1, 4);
        return (frmYear + "-" + frmMon + "-" + frmDay);
    }

    public string GetNewUnionNumber()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetNewUnionNumber";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        string unionno=dbf.ExecuteScalar(cmd);
        cmd.Dispose();
        return unionno;
    }

    public string GetUnionNumber()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetUnionNumber";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        string unionno = dbf.ExecuteScalar(cmd);
        cmd.Dispose();
        return unionno;
    }

    public string DownloadPDF()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_DownloadPDF";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        string pdf=dbf.ExecuteScalar(cmd);
        cmd.Dispose();
        return pdf;
    }

    public static DataTable GetBankDetails()
    {
        DBLibrary.DBFunctions dbf = new DBFunctions();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetBankDetails";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public void UpdatePayeeRefNo()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdatePayeeRefNo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public string GetFormAFormB()
    {
        string IsFormA = "0";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetFormAFormB";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        IsFormA = dbf.ExecuteScalar(cmd);
        cmd.Dispose();

        return IsFormA;
    }

    public string GetStatus()
    {
        string res = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetStatus";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        res = dbf.ExecuteScalar(cmd);
        cmd.Dispose();

        return res;
    }
    public DataTable GetOwnRecordsApplyForMembership()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetOwnRecordsApplyForMembership";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public DataTable GetOwnRecordsUpdateMembership()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetOwnRecordsUpdateMembership";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }


}

