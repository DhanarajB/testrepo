using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;

/// <summary>
/// Summary description for EmpMemberClass
/// </summary>
public class EmpMemberClass
{
	public EmpMemberClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    string _employer = "", _workadd1 = "", _workadd2 = "", _workadd3 = "", _workadd4 = "", _workadd5 = "", _jobtitle = "", _employeeno = "", _isimpactmember = "", _impactmemberdetail = "", _isanotherunionmember = "", _anotherunionmemdet = "", _modifiedby = "";
    string _prevEmployer = "", _prevWorkAdd1 = "", _prevWorkAdd2 = "", _prevWorkAdd3 = "", _prevWorkAdd4 = "", _staffNo = "", _prevJob ="";
    DateTime _dtCommenced;
    Double _annualsalary = 0;
    Double _prevannualsalary = 0;
    bool _IsOldEmployer;
    int _userid = 0;

    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }
    public string Employer
    {
        get { return _employer; }
        set { _employer = value; }
    }
    public string WorkAddress1
    {
        get { return _workadd1; }
        set { _workadd1 = value; }
    }
    public string WorkAddress2
    {
        get { return _workadd2; }
        set { _workadd2 = value; }
    }
    public string WorkAddress3
    {
        get { return _workadd3; }
        set { _workadd3 = value; }
    }
    public string WorkAddress4
    {
        get { return _workadd4; }
        set { _workadd4 = value; }
    }
    public string WorkAddress5
    {
        get { return _workadd5; }
        set { _workadd5 = value; }
    }
    public string JobTitle
    {
        get { return _jobtitle; }
        set { _jobtitle = value; }
    }
    public string EmployeeNo
    {
        get { return _employeeno; }
        set { _employeeno = value; }
    }
    public double AnnualSalary
    {
        get { return _annualsalary; }
        set { _annualsalary = value; }
    }
    public string IsImpactMember
    {
        get { return _isimpactmember; }
        set { _isimpactmember = value; }
    }
    public string ImpactMemberDetail
    {
        get { return _impactmemberdetail; }
        set { _impactmemberdetail = value; }
    }
    public string IsAnotherUnionMember
    {
        get { return _isanotherunionmember; }
        set { _isanotherunionmember = value; }
    }
    public string AnotherUnionMemberDetail
    {
        get { return _anotherunionmemdet; }
        set { _anotherunionmemdet = value; }
    }
    public string ModifiedBy
    {
        get { return _modifiedby; }
        set { _modifiedby = value; }
    }
    
    public string prevEmployer
    {
        get { return _prevEmployer; }
        set { _prevEmployer = value; }
    }
    public string prevWorkAdd1
    {
        get { return _prevWorkAdd1; }
        set { _prevWorkAdd1 = value; }
    }
    public string prevWorkAdd2
    {
        get { return _prevWorkAdd2; }
        set { _prevWorkAdd2 = value; }
    }
    public string prevWorkAdd3
    {
        get { return _prevWorkAdd3; }
        set { _prevWorkAdd3 = value; }
    }
    public string prevWorkAdd4
    {
        get { return _prevWorkAdd4; }
        set { _prevWorkAdd4 = value; }
    }
    public string staffNo
    {
        get { return _staffNo; }
        set { _staffNo = value; }
    }

    public DateTime dtCommenced
    {
        get { return _dtCommenced; }
        set { _dtCommenced = value; }
    }
    public bool IsOldEmployer
    {
        get { return _IsOldEmployer; }
        set { _IsOldEmployer = value; }
    }
    public string PrevJob
    {
        get { return _prevJob; }
        set { _prevJob = value; }
    }
    public double PrevAnnualSalary
    {
        get { return _prevannualsalary; }
        set { _prevannualsalary = value; }
    }
    public void InsertEmpMemDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertEmpMemDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Employer", Employer);
        cmd.Parameters.AddWithValue("@WorkAddress1", WorkAddress1);
        cmd.Parameters.AddWithValue("@WorkAddress2", WorkAddress2);
        cmd.Parameters.AddWithValue("@WorkAddress3", WorkAddress3);
        cmd.Parameters.AddWithValue("@WorkAddress4", WorkAddress4);
        cmd.Parameters.AddWithValue("@WorkAddress5", WorkAddress5);
        cmd.Parameters.AddWithValue("@JobTitle", JobTitle);
        cmd.Parameters.AddWithValue("@EmployeeNo", EmployeeNo);
        cmd.Parameters.AddWithValue("@AnnualSalary", AnnualSalary);
        if (IsImpactMember == null || IsImpactMember=="")
            cmd.Parameters.AddWithValue("@IsImpactMember", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@IsImpactMember", IsImpactMember);

        cmd.Parameters.AddWithValue("@ImpactMemberDetail", ImpactMemberDetail);

        if (IsAnotherUnionMember == null || IsAnotherUnionMember=="")
            cmd.Parameters.AddWithValue("@IsAnotherUnionMember", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@IsAnotherUnionMember", IsAnotherUnionMember);

        cmd.Parameters.AddWithValue("@AnotherUnionMemberDetail", AnotherUnionMemberDetail);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void NOC_InsertChangeOfAdd()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertChangeOfAddNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Employer", Employer);
        cmd.Parameters.AddWithValue("@WorkAddress1", WorkAddress1);
        cmd.Parameters.AddWithValue("@WorkAddress2", WorkAddress2);
        cmd.Parameters.AddWithValue("@WorkAddress3", WorkAddress3);
        cmd.Parameters.AddWithValue("@WorkAddress4", WorkAddress4);
        cmd.Parameters.AddWithValue("@JobTitle", JobTitle);
        cmd.Parameters.AddWithValue("@EmployeeNo", EmployeeNo);
        cmd.Parameters.AddWithValue("@prevWorkAdd1", prevWorkAdd1);
        cmd.Parameters.AddWithValue("@prevWorkAdd2", prevWorkAdd2);
        cmd.Parameters.AddWithValue("@prevWorkAdd3", prevWorkAdd3);
        cmd.Parameters.AddWithValue("@prevWorkAdd4", prevWorkAdd4);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void NOC_InsertEmpMemDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertEmpMemDetailNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Employer", Employer);
        cmd.Parameters.AddWithValue("@WorkAddress1", WorkAddress1);
        cmd.Parameters.AddWithValue("@WorkAddress2", WorkAddress2);
        cmd.Parameters.AddWithValue("@WorkAddress3", WorkAddress3);
        cmd.Parameters.AddWithValue("@WorkAddress4", WorkAddress4);
        cmd.Parameters.AddWithValue("@JobTitle", JobTitle);
        cmd.Parameters.AddWithValue("@EmployeeNo", EmployeeNo);
        cmd.Parameters.AddWithValue("@AnnualSalary", AnnualSalary);
        // new on 11/11/2009 - added by shanthi
        ////cmd.Parameters.AddWithValue("@prevEmployer", prevEmployer);
        ////cmd.Parameters.AddWithValue("@prevWorkAdd1", prevWorkAdd1);
        ////cmd.Parameters.AddWithValue("@prevWorkAdd2", prevWorkAdd2);
        ////cmd.Parameters.AddWithValue("@prevWorkAdd3", prevWorkAdd3);
        ////cmd.Parameters.AddWithValue("@prevWorkAdd4", prevWorkAdd4);
        ////cmd.Parameters.AddWithValue("@staffNo", staffNo);
        //if (dtCommenced.ToString() == "1/1/0001 12:00:00 AM")
        //{
        //    System.Data.SqlTypes.SqlDateTime sqldatenull = System.Data.SqlTypes.SqlDateTime.Null;
        //    cmd.Parameters.AddWithValue("@dtCommenced", sqldatenull);
        //}
        //else
        //    cmd.Parameters.AddWithValue("@dtCommenced", dtCommenced);
        
        // end
        //if (IsImpactMember == null || IsImpactMember == "")
        //    cmd.Parameters.AddWithValue("@IsImpactMember", DBNull.Value);
        //else
        //    cmd.Parameters.AddWithValue("@IsImpactMember", IsImpactMember);

        //cmd.Parameters.AddWithValue("@ImpactMemberDetail", ImpactMemberDetail);

        //if (IsAnotherUnionMember == null || IsAnotherUnionMember == "")
        //    cmd.Parameters.AddWithValue("@IsAnotherUnionMember", DBNull.Value);
        //else
        //    cmd.Parameters.AddWithValue("@IsAnotherUnionMember", IsAnotherUnionMember);

        //cmd.Parameters.AddWithValue("@AnotherUnionMemberDetail", AnotherUnionMemberDetail);
        cmd.Parameters.AddWithValue("@IsOldEmployer", IsOldEmployer);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void UpdateEmpMemDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateEmpMemDetail";
        cmd.CommandType = CommandType.StoredProcedure; 
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Employer", Employer);
        cmd.Parameters.AddWithValue("@WorkAddress1", WorkAddress1);
        cmd.Parameters.AddWithValue("@WorkAddress2", WorkAddress2);
        cmd.Parameters.AddWithValue("@WorkAddress3", WorkAddress3);
        cmd.Parameters.AddWithValue("@WorkAddress4", WorkAddress4);
        cmd.Parameters.AddWithValue("@WorkAddress5", WorkAddress5);
        cmd.Parameters.AddWithValue("@JobTitle", JobTitle);
        cmd.Parameters.AddWithValue("@EmployeeNo", EmployeeNo);
        cmd.Parameters.AddWithValue("@AnnualSalary", AnnualSalary);
        if (IsImpactMember == null || IsImpactMember == "")
            cmd.Parameters.AddWithValue("@IsImpactMember", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@IsImpactMember", IsImpactMember);
        
        cmd.Parameters.AddWithValue("@ImpactMemberDetail", ImpactMemberDetail);
        
        if (IsAnotherUnionMember == null || IsAnotherUnionMember == "")
            cmd.Parameters.AddWithValue("@IsAnotherUnionMember", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@IsAnotherUnionMember", IsAnotherUnionMember);

        cmd.Parameters.AddWithValue("@AnotherUnionMemberDetail", AnotherUnionMemberDetail);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);

        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void NOC_UpdateChangeOfAdd()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateChangeOfAddNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Employer", Employer);
        cmd.Parameters.AddWithValue("@WorkAddress1", WorkAddress1);
        cmd.Parameters.AddWithValue("@WorkAddress2", WorkAddress2);
        cmd.Parameters.AddWithValue("@WorkAddress3", WorkAddress3);
        cmd.Parameters.AddWithValue("@WorkAddress4", WorkAddress4);
        cmd.Parameters.AddWithValue("@JobTitle", JobTitle);
        cmd.Parameters.AddWithValue("@EmployeeNo", EmployeeNo);
        cmd.Parameters.AddWithValue("@prevWorkAdd1", prevWorkAdd1);
        cmd.Parameters.AddWithValue("@prevWorkAdd2", prevWorkAdd2);
        cmd.Parameters.AddWithValue("@prevWorkAdd3", prevWorkAdd3);
        cmd.Parameters.AddWithValue("@prevWorkAdd4", prevWorkAdd4);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void NOC_UpdateEmpMemDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateEmpMemDetailNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Employer", Employer);
        cmd.Parameters.AddWithValue("@WorkAddress1", WorkAddress1);
        cmd.Parameters.AddWithValue("@WorkAddress2", WorkAddress2);
        cmd.Parameters.AddWithValue("@WorkAddress3", WorkAddress3);
        cmd.Parameters.AddWithValue("@WorkAddress4", WorkAddress4);
        cmd.Parameters.AddWithValue("@JobTitle", JobTitle);
        cmd.Parameters.AddWithValue("@EmployeeNo", EmployeeNo);
        cmd.Parameters.AddWithValue("@AnnualSalary", AnnualSalary);
        // new on 11/11/2009 - added by shanthi
        //cmd.Parameters.AddWithValue("@prevEmployer", prevEmployer);
        //cmd.Parameters.AddWithValue("@prevWorkAdd1", prevWorkAdd1);
        //cmd.Parameters.AddWithValue("@prevWorkAdd2", prevWorkAdd2);
        //cmd.Parameters.AddWithValue("@prevWorkAdd3", prevWorkAdd3);
        //cmd.Parameters.AddWithValue("@prevWorkAdd4", prevWorkAdd4);
        //cmd.Parameters.AddWithValue("@staffNo", staffNo);
        
        //if (dtCommenced.ToString() == "1/1/0001 12:00:00 AM")
        //{
        //    System.Data.SqlTypes.SqlDateTime sqldatenull = System.Data.SqlTypes.SqlDateTime.Null;
        //    cmd.Parameters.AddWithValue("@dtCommenced", sqldatenull);
        //}
        //else
        //    cmd.Parameters.AddWithValue("@dtCommenced", dtCommenced);
        
        // end
        //if (IsImpactMember == null || IsImpactMember == "")
        //    cmd.Parameters.AddWithValue("@IsImpactMember", DBNull.Value);
        //else
        //    cmd.Parameters.AddWithValue("@IsImpactMember", IsImpactMember);

        //cmd.Parameters.AddWithValue("@ImpactMemberDetail", ImpactMemberDetail);

        //if (IsAnotherUnionMember == null || IsAnotherUnionMember == "")
        //    cmd.Parameters.AddWithValue("@IsAnotherUnionMember", DBNull.Value);
        //else
        //    cmd.Parameters.AddWithValue("@IsAnotherUnionMember", IsAnotherUnionMember);

        
        //cmd.Parameters.AddWithValue("@AnotherUnionMemberDetail", AnotherUnionMemberDetail);
        cmd.Parameters.AddWithValue("@IsOldEmployer", IsOldEmployer);
        cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);

        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public void NOC_UpdateOldEmpDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_UpdateOldEmpDetailNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@prevEmployer", prevEmployer);
        cmd.Parameters.AddWithValue("@prevWorkAdd1", prevWorkAdd1);
        cmd.Parameters.AddWithValue("@prevWorkAdd2", prevWorkAdd2);
        cmd.Parameters.AddWithValue("@prevWorkAdd3", prevWorkAdd3);
        cmd.Parameters.AddWithValue("@prevWorkAdd4", prevWorkAdd4);
        cmd.Parameters.AddWithValue("@staffNo", staffNo);
        cmd.Parameters.AddWithValue("@PrevJob", PrevJob);
        cmd.Parameters.AddWithValue("@PrevSalary", PrevAnnualSalary);
        cmd.Parameters.AddWithValue("@SessionID", HttpContext.Current.Session.SessionID);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }

    public void NOC_InsertOldEmpDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_InsertOldEmpDetailNOC";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@prevEmployer", prevEmployer);
        cmd.Parameters.AddWithValue("@prevWorkAdd1", prevWorkAdd1);
        cmd.Parameters.AddWithValue("@prevWorkAdd2", prevWorkAdd2);
        cmd.Parameters.AddWithValue("@prevWorkAdd3", prevWorkAdd3);
        cmd.Parameters.AddWithValue("@prevWorkAdd4", prevWorkAdd4);
        cmd.Parameters.AddWithValue("@staffNo", staffNo);
        cmd.Parameters.AddWithValue("@PrevJob", PrevJob);
        cmd.Parameters.AddWithValue("@PrevSalary", PrevAnnualSalary);
        dbf.ExecuteNonQuery(cmd);
        cmd.Dispose();
    }
    public DataTable GetEmpMemDetail()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetEmpMemDetail";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }

    public bool IsExistEmpMem()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_IsExistEmpMem";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserID", UserID);
        int id = int.Parse(dbf.ExecuteScalar(cmd));
        cmd.Dispose();

        bool res=false;
        if (id != 0)
            res = true;
        else
            res = false;
        
        return res;
    }
}