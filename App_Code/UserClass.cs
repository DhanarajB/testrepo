using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DBLibrary;
/// <summary>
/// Summary description for UserClass
/// </summary>
public class UserClass
{
	public UserClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DBLibrary.DBFunctions dbf = new DBFunctions();
    public DataTable GetUserList()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "spl_GetUserList";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dbf.ExecuteDTQuery(cmd);
        cmd.Dispose();
        return dt;
    }
}
