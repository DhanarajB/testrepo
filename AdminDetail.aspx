<%@ Page Language="C#" MasterPageFile="~/MasterMain.master" AutoEventWireup="true"
    CodeFile="AdminDetail.aspx.cs" Inherits="AdminDetail" Title="Impact Membership Application" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    function FormValidation()
    {
        var i=0;
        var ErrMsg = '';
        var CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>'
        
        if(document.getElementById('<%=txtCurSubRate.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "The current rate of union subscription"</td></tr>';
            CommonMsg +='<tr><td>The current rate of union subscription</td></tr>';
        }
        
        if(document.getElementById('<%=txtBasicSalary.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Basic Salary"</td></tr>';
            CommonMsg +='<tr><td>Basic Salary</td></tr>';
        }
        
        if(document.getElementById('<%=txtBranch.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Branch"</td></tr>';
            CommonMsg +='<tr><td>Branch</td></tr>';
        }
        
        if(document.getElementById('<%=txtMemberName.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Name"</td></tr>';
            CommonMsg +='<tr><td>Name</td></tr>';
        }
        
        if(document.getElementById('<%=txtApprovedDate.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Date Approved" (dd/mm/yyyy)</td></tr>';                 
            CommonMsg +='<tr><td>Date Approved (dd/mm/yyyy)</td></tr>';
        }
        if(document.getElementById('<%=txtApprovedPhNo.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Phone No"</td></tr>';
            CommonMsg +='<tr><td>Phone No</td></tr>';
        }
        if(document.getElementById('<%=txtSODate.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Date SO" (dd/mm/yyyy)</td></tr>';
            CommonMsg +='<tr><td>Date SO (dd/mm/yyyy)</td></tr>';
        }
        if(document.getElementById('<%=txtProcessedBy.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Processed By"</td></tr>';
            CommonMsg +='<tr><td>Processed By</td></tr>';
        }
        if(document.getElementById('<%=txtProcessDate.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Date" (dd/mm/yyyy)</td></tr>';
            CommonMsg +='<tr><td>Date (dd/mm/yyyy)</td></tr>';
        }
        if(document.getElementById('<%=txtApprovedDate.ClientID%>').value!="")
        {
            if(!isValidateDate(document.getElementById('<%=txtApprovedDate.ClientID%>')))
            {
                i++;
                ErrMsg += '<tr><td>Invalid date approved (dd/mm/yyyy)</td></tr>';
                CommonMsg +='<tr><td>Invalid date approved (dd/mm/yyyy)</td></tr>';
            }
        }
        if(document.getElementById('<%=txtSODate.ClientID%>').value!="")
        {
            if(!isValidateDate(document.getElementById('<%=txtSODate.ClientID%>')))
            {
                i++;
                ErrMsg += '<tr><td>Invalid date SO (dd/mm/yyyy)</td></tr>';
                CommonMsg +='<tr><td>Invalid date SO (dd/mm/yyyy)</td></tr>';
            }
        }
        if(document.getElementById('<%=txtProcessDate.ClientID%>').value!="")
        {
            if(!isValidateDate(document.getElementById('<%=txtProcessDate.ClientID%>')))
            {
                i++;
                ErrMsg += '<tr><td>Invalid date (dd/mm/yyyy)</td></tr>';
                CommonMsg +='<tr><td>Invalid date (dd/mm/yyyy)</td></tr>';
            }
        }


        if (ErrMsg != '')
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        return true;
    }
</script>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="background-color: #de8518; border-bottom: 4px solid #9a5808;">
                            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 6px; padding-top: 6px;
                                float: left;">
                                <img src="images/Heading_bullet.jpg" alt="Impact" /></div>
                            <div class="PageHeading">
                                Administrative Details</div>
                            <div class="back_to_list">
                                &laquo;&nbsp;<a href="UsersList.aspx">Back to List</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #de8518;
                    border-right: 1px solid #de8518; border-bottom: 1px solid #de8518">
                    <tr>
                        <td align="center" valign="top" style="padding: 20px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="admin_table ">
                                <tr>
                                    <td align="left" valign="top" class="admin_table_h1">
                                        SUBSCRIPTION DETAILS</td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" style="padding: 20px;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="74%" align="left" valign="top" class="declaration_txt">
                                                    The current rate of union subscription determined in accordance with Union rules
                                                    for the above member is</td>
                                                <td width="26%" align="left" valign="top">
                                                    <input name="txtCurSubRate" type="text" class="textbox" id="txtCurSubRate" runat="server" />
                                                    &nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                            <tr>
                                                <td width="56%" align="left" valign="top" class="declaration_txt">
                                                    % of basic salary subject to maximum subscription based on a basic salary of &euro;</td>
                                                <td width="44%" align="left" valign="middle">
                                                    <input name="txtBasicSalary" type="text" class="textbox" id="txtBasicSalary" runat="server" />
                                                    &nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="admin_table ">
                                <tr>
                                    <td align="left" valign="top" class="admin_table_h1">
                                        IMPACT ACCOUNT DETAILS</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding: 20px;">
                                        <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                            <tr>
                                                <td width="25%" align="right" valign="top" class="content">
                                                    IMPACT Account Name</td>
                                                <td width="43%" align="left" valign="top">
                                                    <asp:Label ID="lblImpAccName" runat="server" CssClass="lblCss"></asp:Label>
                                                </td>
                                                <td width="11%" align="right" valign="top" class="content">
                                                    Account No</td>
                                                <td width="21%" align="left" valign="top">
                                                    <asp:Label ID="lblAccountNo" runat="server" CssClass="lblCss"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    Bank</td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblBank" runat="server" CssClass="lblCss"></asp:Label></td>
                                                <td align="right" valign="top" class="content">
                                                    Sort Code</td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblSorCode" runat="server" CssClass="lblCss"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    Bank Address</td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblBankAddress" runat="server" CssClass="lblCss"></asp:Label></td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td align="left" valign="top">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    Payee Reference No</td>
                                                <td align="left" valign="top">
                                                    <input name="txtPayeeRefNo" type="text" class="textbox" id="txtPayeeRefNo" runat="server" /></td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td align="left" valign="top">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="admin_table ">
                                <tr>
                                    <td align="left" valign="top" class="admin_table_h1">
                                        MEMBERSHIP APPROVAL - BRANCH</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding: 20px;">
                                        <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                            <tr>
                                                <td width="25%" align="right" valign="top" class="content">
                                                    Branch</td>
                                                <td width="25%" align="left" valign="top">
                                                    <input name="txtBranch" type="text" class="textbox" id="txtBranch" runat="server" /></td>
                                                <td width="25%" align="right" valign="top" class="content">
                                                    Date Approved</td>
                                                <td width="25%" align="left" valign="top">
                                                    <input name="txtApprovedDate" type="text" class="textbox" id="txtApprovedDate" runat="server" />
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    Name</td>
                                                <td align="left" valign="top">
                                                    <input name="txtMemberName" type="text" class="textbox" id="txtMemberName" runat="server" /></td>
                                                <td align="right" valign="top" class="content">
                                                    Phone No</td>
                                                <td align="left" valign="top">
                                                    <input name="txtApprovedPhNo" type="text" class="textbox" id="txtApprovedPhNo" runat="server" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right" valign="top" class="content">
                                                    The Applicant has been approved as a member by the above Branch</td>
                                                <td align="left" valign="top">
                                                    <input type="checkbox" name="chkBranch" id="chkBranch" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="admin_table ">
                                <tr>
                                    <td align="left" valign="top" class="admin_table_h1">
                                        IMPACT HEAD OFFICE USE</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding: 20px;">
                                        <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                            <tr>
                                                <td width="25%" align="right" valign="top" class="content">
                                                    Date SO or Detuction at source from sent to Bank/Employer</td>
                                                <td width="25%" align="left" valign="top">
                                                    <input name="txtSODate" type="text" class="textbox" id="txtSODate" runat="server" /></td>
                                                <td width="25%" align="right" valign="top" class="content">
                                                    Date</td>
                                                <td width="25%" align="left" valign="top">
                                                    <input name="txtProcessDate" type="text" class="textbox" id="txtProcessDate" runat="server" />
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="content">
                                                    Processed by</td>
                                                <td align="left" valign="top">
                                                    <input name="txtProcessedBy" type="text" class="textbox" id="txtProcessedBy" runat="server" /></td>
                                                <td align="right" valign="top">
                                                    &nbsp;</td>
                                                <td align="left" valign="top">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top" class="content" style="padding-right: 15px;">
                            &nbsp;Approved
                            <input type="checkbox" name="chkStatus" id="chkStatus" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMsg" runat="server" CssClass="ErrorMsg" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top" style="padding-right: 15px; padding-bottom: 10px;
                            padding-top: 10px;">
                            &nbsp;&nbsp;
                            <asp:Button ID="btnPrevious" runat="server" Text="&laquo;&nbsp; Previous" CssClass="button" OnClick="btnPrevious_Click" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" OnClientClick="return FormValidation()" />
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
