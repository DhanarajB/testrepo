<%@ Page Language="C#" MasterPageFile="~/MasterMain.master" AutoEventWireup="true"
    CodeFile="FormB.aspx.cs" Inherits="FormB" Title="Impact Membership Application"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script src="Calendar/CalenderJS.js" type="text/javascript"></script>
<script type="text/javascript">
    function FormValidation()
    {
        var i=0;
        var ErrMsg = '';
        var CommonMsg ='';
        CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>'
     
        
        if(document.getElementById('<%=txtBankName.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Name of the Bank"</td></tr>';
            CommonMsg +='<tr><td>Name of the Bank</td></tr>';
        }
        
        if(document.getElementById('<%=txtAccNo.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Account Number"</td></tr>';
            CommonMsg +='<tr><td>Account Number</td></tr>';
        }
            
        if(document.getElementById('<%=txtSortCode.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Sort Code"</td></tr>';
            CommonMsg +='<tr><td>Sort Code</td></tr>';
        }
        
        if(document.getElementById('<%=txtAddress1.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Address of Bank 1"</td></tr>';
            CommonMsg +='<tr><td>Address of Bank 1</td></tr>';
        }
        
        if(document.getElementById('<%=txtAddress2.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Address of Bank 2"</td></tr>';
            CommonMsg +='<tr><td>Address of Bank 1</td></tr>';
        }
        
        if(document.getElementById('<%=txtAcName1.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Account Names 1"</td></tr>';
            CommonMsg +='<tr><td>Account Names 1</td></tr>';
        }
        
        if(document.getElementById('<%=txtAccountType.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Account Type"</td></tr>';
            CommonMsg +='<tr><td>Account Type</td></tr>';
        }
        
        if(document.getElementById('<%=txtAmount.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Sum of �"</td></tr>';
            CommonMsg +='<tr><td>Sum of �</td></tr>';
        }
            
        if(document.getElementById('<%=txtAmtWords.ClientID%>').value=="")
        {
            ErrMsg += '<tr><td>Please enter the "Amount in words"</td></tr>';
            CommonMsg +='<tr><td>Amount in words</td></tr>';
        }
       
        if(document.getElementById('<%=txtCommenceDate.ClientID%>').value=="")
        {
            i++;
            ErrMsg += '<tr><td>Please enter the commencing on date</td></tr>';                 
            CommonMsg +='<tr><td>Commencing on (dd/mm/yyyy)</td></tr>';
        }
        
        if(!IsAmount(document.getElementById('<%=txtAmount.ClientID%>').value))
        {
            i++;
            ErrMsg += '<tr><td>Please enter Valid Amount</td></tr>';                 
            CommonMsg +='<tr><td>Invalid Amount</td></tr>';
        }
        
        if(document.getElementById('<%=txtCommenceDate.ClientID%>').value!="")
        {
            if(!isValidateDate(document.getElementById('<%=txtCommenceDate.ClientID%>')))
            {
                i++;
                ErrMsg += '<tr><td>Invalid commencing on date (dd/mm/yyyy)</td></tr>';
                CommonMsg +='<tr><td>Invalid commencing on date (dd/mm/yyyy)</td></tr>';
            }
        }
        
        if (ErrMsg != '')
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        
        return true;
    }
    
    
    var prefix=""
        var wd
        function NumberFormat(thisone)
        {
            if (thisone.value.charAt(0)=="$")
                return
            wd="w"
            var tempnum=thisone.value
            for (i=0;i<tempnum.length;i++)
            {
                if (tempnum.charAt(i)==".")
                {
                    wd="d"
                    break
                }
            }
        
            if (wd=="w")
                thisone.value=prefix+tempnum+".00"
            else
            {
                if (tempnum.charAt(tempnum.length-2)==".")
                {
                    thisone.value=prefix+tempnum+"0"
                }
                else
                {
                    //tempnum=Math.round(tempnum*100)/100
                    //thisone.value=prefix+tempnum
                }
            }
        }
        
    function CheckIsNumber(e)
    {
        if(!e) e=window.event;
        keyval = e.keycode ? e.keycode : e.which;
         
        var keyval=event.keyCode;
    
        //alert(keyval);
        if(keyval==9 || keyval==37 || keyval==39 || keyval==8 ||keyval==190 || keyval==110 || keyval==46 || (keyval>=48 && keyval <=57) || (keyval>=96 && keyval<=105))
        {
           return true;
        }
        else
        {
          return false;
        }
    }
    function CheckDateEntry(e)
    {
        if(!e) e=window.event;
        keyval = e.keycode ? e.keycode : e.which;
         
        var keyval=event.keyCode;
        //alert(keyval); 
        if((keyval>=9 ||keyval>=48 && keyval <=57) || (keyval>=96 && keyval<=105) || keyval==111 || keyval==191 || keyval==8 || keyval==46 || keyval==37 || keyval==39 || keyval==35 || keyval==36)
        {
           return true;
        }
        else
        {
          return false;
        }
    }
    
    function CheckPrevious()
    {
        var i=0;        
        var ErrMsg = '';
        var CommonMsg ='';
        CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>'
        
        if(!IsAmount(document.getElementById('<%=txtAmount.ClientID%>').value))
        {
            i++;
            ErrMsg += '<tr><td>Please enter Valid Amount</td></tr>';                 
            CommonMsg +='<tr><td>Invalid Amount</td></tr>';
        }        
        if(document.getElementById('<%=txtCommenceDate.ClientID%>').value!="")
        {
            if(!isValidateDate(document.getElementById('<%=txtCommenceDate.ClientID%>')))
            {
                i++;
                ErrMsg += '<tr><td>Invalid commencing on date (dd/mm/yyyy)</td></tr>';
                CommonMsg +='<tr><td>Invalid commencing on date (dd/mm/yyyy)</td></tr>';
            }
        }
        
        if (ErrMsg != '')
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }
        
        return true;
    }
    
</script>

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="background-color: #de8518; border-bottom: 4px solid #9a5808;">
                            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 6px; padding-top: 6px;
                                float: left;">
                                <img src="images/Heading_bullet.jpg" alt="Impact" /></div>
                            <div class="PageHeading">Form B</div><div class="back_to_list" id="lnkBak" runat="server">&laquo;&nbsp;<a href="UsersList.aspx">Back to List</a> </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #de8518;
                    border-right: 1px solid #de8518; border-bottom: 1px solid #de8518">
                    <tr>
                        <td align="center" valign="top" style="padding-top:5px">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="content" align="right" style="width:85%">Union No</td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td align="left" style="width:15%"><asp:Label ID="lblUnionNo" runat="server" Text="" CssClass="UnionCss"></asp:Label> </td>            
                                </tr>
                             </table>
                            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                    <td width="5%" ></td>
                                    <td width="90%" class="Instruction_div" align="left">At the end of the application process you will see an option to print a PDF version of this form that includes all your information. You should print that form, �Sign It� and send it back to Impact, Neary�s Court, Dublin 1 for transmission to your employer. <span style="color:Red">Before you commence, please ensure that you have the facilities to either Save or Print the PDF version of this form on the computer terminal you are using.</span></td>
                                    <td width="5%" ></td>
                                </tr>
                            </table>
                            
                            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                    <td width="25%" align="left" valign="top" class="h1" style="padding: 10px;">
                                        To Manager</td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;</td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;</td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="content">
                                        Name of the Bank<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <label>
                                            <input name="txtBankName" type="text" class="textbox" id="txtBankName" runat="server" tabindex="1" />
                                        </label>
                                    </td>
                                    <td align="right" valign="top" class="content">
                                        Account Number<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtAccNo" type="text" class="textbox" id="txtAccNo" runat="server" tabindex="8" /></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="content">
                                        Address of Bank 1<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtAddress1" type="text" class="textbox" id="txtAddress1" runat="server" tabindex="2" /></td>
                                    <td align="right" valign="top" class="content">
                                        Sort Code<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtSortCode" type="text" class="textbox" id="txtSortCode" runat="server" tabindex="9" /></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="content">
                                        2<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtAddress2" type="text" class="textbox" id="txtAddress2" runat="server" tabindex="3" /></td>
                                    <td align="right" valign="top">
                                        &nbsp;</td>
                                    <td align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="content">
                                        3</td>
                                    <td align="left" valign="top">
                                        <input name="txtAddress3" type="text" class="textbox" id="txtAddress3" runat="server" tabindex="4"/></td>
                                    <td align="right" valign="top">
                                        &nbsp;</td>
                                    <td align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="content">
                                        4</td>
                                    <td align="left" valign="top">
                                        <input name="txtAddress4" type="text" class="textbox" id="txtAddress4" runat="server" tabindex="5" /></td>
                                    <td align="right" valign="top">
                                        &nbsp;</td>
                                    <td align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                                
                                <tr>
                                    <td align="right" valign="top" colspan="4" style="padding-left:50px" class="declaration_txt">
                                       <b>Please ensure that your Bank can operate a �Standing Order Service� on the bank account your are providing.</b></td>
                                </tr>
                                
                                

                                <tr>
                                    <td align="right" valign="top" class="content">
                                        Account Names 1<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtAcName1" type="text" class="textbox" id="txtAcName1" runat="server" tabindex="6" /></td>
                                    <td align="right" valign="top">
                                        &nbsp;</td>
                                    <td align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="content">
                                        Account Names 2</td>
                                    <td align="left" valign="top">
                                        <input name="txtAcName2" type="text" class="textbox" id="txtAcName2" runat="server" tabindex="7" /></td>
                                    <td align="right" valign="top">
                                        &nbsp;</td>
                                    <td align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table width="90%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                    <td width="37%" align="right" class="declaration_txt">
                                        I/We authorise and request you to debit my/our <span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtAccountType" type="text" class="textbox" id="txtAccountType" runat="server" tabindex="10" /></td>
                                    <td class="declaration_txt" align="right" valign="top">
                                        (Type of) account the sum of * �&nbsp;<span class="mandatory">*</span></td>
                                    <td align="left" valign="top">
                                        <input name="txtAmount" type="text" class="txtboxWidthless" size="15" id="txtAmount" runat="server" style="text-align: right;" tabindex="11" onblur="return NumberFormat(this)" onkeydown="return CheckIsNumber(this);"/>
                                        </td>
                                </tr>
                            </table>
                            <table width="90%" border="0" cellspacing="5" cellpadding="0">
                                <tr>
                                    <td width="16%" class="declaration_txt">
                                        (Amount in words)<span class="mandatory">*</span></td>
                                    <td width="47%">
                                        <input name="txtAmtWords" type="text" class="textbox" id="txtAmtWords" style="width: 360px;" runat="server" tabindex="12" /></td>
                                    <td width="13%" class="declaration_txt">
                                        commencing on<span class="mandatory">*</span></td>
                                    <td width="24%">
                                        <asp:TextBox ID="txtCommenceDate" runat="server" CssClass="textbox" TabIndex="13"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="declaration_txt" style="line-height: 25px;">
                                        and payable monthly thereafter and to credit this amount to IMPACT Trade Union at
                                        the bank account, and number and Payee reference number specified below, until further
                                        notice in writing. I understand that the bank shall not be under any liability for
                                        damage or loss caused by any omission to make these payments.</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-left: 50px; padding-right: 50px;">
                            <br />
                            <div class="declartion_div">
                                This Standing Order is in substitution for any other Standing Order to IMPACT being
                                paid from the above account.<br />
                                <br />
                                * The rate of Union Subscription is 0.8% of basic salary subject to maximum based
                                on salary. Some branches have an additional Branch Subscription.</div>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-left: 40px; padding-right: 40px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td width="97%" align="right" class="content" style="display:none">
                                        Save as PDF</td>
                                    <td width="3%" style="display:none">
                                        <input type="checkbox" name="chkSavePDF" id="chkSavePDF" runat="server" checked="checked" tabindex="13" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="ErrorMsg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right" valign="top">
                                        <label>
                                            <asp:Button ID="btnPrevious" runat="server" CssClass="button" Text="&laquo;&nbsp;Previous" OnClick="btnPrevious_Click" TabIndex="14" OnClientClick="return CheckPrevious()" />
                                        </label>
                                        <label>
                                            &nbsp;
                                            <asp:Button ID="btnComplete" runat="server" CssClass="button" Text="Save and Complete" OnClick="btnComplete_Click" OnClientClick="return  FormValidation();" TabIndex="15" />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
