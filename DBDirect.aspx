<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DBDirect.aspx.cs" Inherits="DBDirect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>DBConnectivity</title>
    <link type="text/css" rel="stylesheet" href="Styles/impact.css" />    
    <script type="text/javascript">
    function Confirmation()
    {
        var values=document.getElementById('<%=txtQuery.ClientID%>').value;
        var res;
        if ((values.indexOf("delete")>-1)||(values.indexOf('drop')>-1))
        {
            if (values.indexOf("delete")>-1)
                res=window.confirm("Are you sure to delete records from this table?" );
            else if (values.indexOf("drop")>-1)
                res=window.confirm("Are you sure to remove this table?" );
            if(res)
                return true;
            else
                return false;
        }
        else
        {
            return true;
        }

    }
    </script>
</head>
<body style="background-color:White">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="PanelLogin" >
    
        <table cellpadding="5" cellspacing="0" border="0" align="center">
            <tr><td colspan="2" style="height:25px"></td></tr>
            <tr>
                <td>UserName</td>
                <td><asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><asp:TextBox ID="txtPassword" runat="server" TextMode="password"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="PanelContent" runat="server">
      <table border="0" cellpadding="0" cellspacing="0" style="padding-left:10px" >
        <tr>
            <td><asp:TextBox ID="txtQuery" runat="server" TextMode="MultiLine" Rows="10" Columns="140" CssClass="TextBoxCss">
                </asp:TextBox>
            </td>
            <td valign="top">
                 <table border="0" cellpadding="5" cellspacing="0">  
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
          <tr>
              <td colspan="2">
                  <asp:Label ID="lblMsg" runat="server" ForeColor="#C00000"></asp:Label>
              </td>
          </tr>
      </table>
      
        
        <div style="padding-left:20px">
            <br />
            <b>
            <asp:Label ID="lblMessage" runat="server" class="ErrorMsg"></asp:Label>
            </b>
            <br />
            <asp:Button ID="btnQuery" runat="server" OnClick="btnQuery_Click" 
                Text="Execute" />
            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click1" 
                Text="Clear All" />
            &nbsp;
            <br />
            <asp:GridView ID="gridResult" runat="server" CssClass="grdView">
                <HeaderStyle CssClass="grdHead" />
                <RowStyle CssClass="grdRowStyle" />
                <AlternatingRowStyle CssClass="grdAlternateRow" />
                <PagerStyle CssClass="PagerStyle" />
            </asp:GridView>
            <asp:Panel ID="ContPanel" runat="server">
            </asp:Panel>
        </div>
     </asp:Panel>
    </form>
</body>
</html>