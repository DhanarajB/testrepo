<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Email.aspx.cs" Inherits="Email" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link href="styles/impact.css" rel="stylesheet" type="text/css" />
<script src="Script/MsgBoxFunction.js" type="text/javascript"></script>
<script src="Script/CommonJS.js" type="text/javascript"></script>
    <title>Impact Membership Application</title>
    <style type="text/css">
.highlight{color:#9a5808}
    .style1
    {
        width: 100%;
    }
</style>
<script type="text/javascript">
    function  validateEmail()
    {   
        var i=0;
        var ErrMsg = '';
        var CommonMsg = '<tr><td height="30px">Please enter data for the following fields</td></tr>';
    
        var emailid=document.getElementById('<%=txtEmail.ClientID%>').value ;      
        if(emailid == "" || emailid == null)
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Email"</td></tr>';
            CommonMsg +='<tr><td>E-mail</td></tr>';
        }
                       
        var cnfmemailid=document.getElementById('<%=txtcnfmEmail.ClientID%>').value;        
        if(cnfmemailid == "" || cnfmemailid == null)
        {
            i++;
            ErrMsg += '<tr><td>Please enter the "Re-enter E-mail"</td></tr>';
            CommonMsg +='<tr><td>Re-Enter E-mail</td></tr>';
        }    
        
        if(emailid!="" && cnfmemailid!="")
        {
            if(emailid != cnfmemailid)
            {
                i++;
                ErrMsg += '<tr><td>Both e-mail should be same</td></tr>';
                CommonMsg +='<tr><td>Both e-mail should be same</td></tr>';
            }
        }
        
        // if(emailid!= "" && cnfmemailid!= "")
        //{
          // var emailPat = /^(\".*\"|[A-Za-z]\w*)@(\[\d{1,3}(\.\d{1,3}){3}]|[A-Za-z]\w*(\.[A-Za-z]\w*)+)$/;
          // var email = document.getElementById('<%=txtEmail.ClientID  %>').value;
           //var cnfmemail = document.getElementById('<%=txtcnfmEmail.ClientID  %>').value;
           //if ((email != null && email != "") || (cnfmemail != null && cnfmemail != "")) {
             //  var matcharray = email.match(emailPat);
             //  var matcharray1 = cnfmemail.match(emailPat);
              // if ((matcharray == null) || (matcharray1 == null)) {                                 
                //   CommonMsg +='<tr><td>Email Address is invalid</td></tr>';                                                 
               //}              
          // }
        //}
        
        var emailid=document.getElementById('<%=txtEmail.ClientID%>').value;
        if(emailid!="")
        {
            if(!isEmail(emailid))
            {
                i++;
                ErrMsg += '<tr><td>Please enter the valid E-mail</td></tr>';
                CommonMsg +='<tr><td>Given E-mail is invalid</td></tr>';
            }
        }
        
        
        /*if((emailid == "" || emailid == null) || (cnfmemailid == "" || cnfmemailid == null) || (emailid != cnfmemailid))
        {            
            MsgBox(1,'<table>' + CommonMsg + '</table>');           
            return false;   
        }
        else
        {
            return true;
        }  */
       
        if (ErrMsg != '') 
        {
            if(i>1)
                MsgBox(1,'<table>' + CommonMsg + '</table>');
            else
                MsgBox(1,'<table>' + ErrMsg + '</table>');
            return false;   
        }                             
    }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="950" border="0" cellspacing="0" cellpadding="0" class="main_container">
  <tr>
    <td align="left" valign="top" style="background-color:#3d3d3d; border-left:8px solid #585858;border-right:8px solid #585858; height:5px; "><img alt="" src="images/spacer.gif" /></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" valign="top" style="padding-left:30px; padding-top:20px; padding-bottom:20px;"><img src="images/impact_logo.jpg" alt="Impact" width="147" height="48" /></td>
            <td align="right" valign="top" style=" padding-top:40px; ">
                <table width="30%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center" valign="top" style="padding-top:3px;"><img src="images/home_icon.jpg" alt="Home" width="11" height="9" align="middle" /></td>
                    <td align="left" valign="top" style="padding-left:4px; padding-right:10px; "><a href="Index.aspx">
                        Home</a></td>
                  </tr>
                </table>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="left" valign="top" style="padding-left:15px; padding-right:15px;" >
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >                          
                          <tr>
                            <td colspan="2" align="right" valign="top" ><br />
                                     <table align="right"  border="0" cellspacing="0" cellpadding="0" id="tblUser" runat="server">
                                        <tr>
                                            <td align="right" valign="top" id="tdHeadingSpace" runat="server">
                                                <img src="images/heading_bg.jpg" alt="Impact" width="33" height="29" /></td>
                                            <td id="tdApplication" runat="server" align="left" valign="top" class="heading_bg_new">
                                                APPLICATION FOR MEMBERSHIP</td>
                                        </tr>
                                      </table>
                           </td>
                           </tr>
                            <tr>
        <td colspan="2" style="border:1px solid #e19839;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                                                    <td colspan="2" >
                                                                 <table align="center" >
                                                                    <tr><td colspan="2" height="50"></td></tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="content" style="display:none">Please enter your E-Mail Address here</span>
                                                                            <asp:Label ID="Label1" runat="server" Text="Please enter your E-Mail Address here" CssClass="content"></asp:Label><span style="color:Red">*</span>
                                                                        </td>
                                                                        <td><asp:TextBox ID="txtEmail" runat="server" class="textbox"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="content" style="display:none">Please enter your E-Mail Address here</span>
                                                                            <asp:Label ID="Label3" runat="server" Text="Re-enter E-mail" CssClass="content"></asp:Label><span style="color:Red">*</span>
                                                                        </td>
                                                                        <td><asp:TextBox ID="txtcnfmEmail" runat="server" class="textbox"></asp:TextBox></td>
                                                                    </tr>  
                                                                    <tr><td height="20" colspan="2"></td></tr>                                                                                                                                      
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:Label ID="Label2" runat="server" Text="Note:" Font-Bold="true"></asp:Label><span style="color:Red">*</span>
                                                                            <asp:Label ID="Label4" runat="server" Text="Preferably user your personal and not work e-mail"></asp:Label>
                                                                        </td>
                                                                            
                                                                        
                                                                    </tr>
                                                                </table>
                                                       </td>
                                       </tr>
                                       <tr><td colspan="2" height="100"></td></tr>
                                       <tr><td colspan="2" align="right"><div style="padding-right:100px;"><asp:Button ID="Btnnext" Text="Next �" runat="server" CssClass="button"  OnClick="Btnnext_Click" OnClientClick="return validateEmail();"/></div> </td></tr>
                                       <tr><td colspan="2" height="100"></td></tr>
                                       </table>
           </td>
      </tr>
       
                           
         </table>
           
        </td>
      </tr>
     
      <tr>
        <td align="left" valign="top" class="footer" >�2009 Copyrights Impact Trade Union</td>
      </tr>
      <tr>
        <td align="left" height="30"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="background-color:#3d3d3d; border-left:8px solid #585858;border-right:8px solid #585858; height:8px; "><img src="images/spacer.gif" alt=""/></td>
  </tr>
</table>
    </form>
</body>
</html>
