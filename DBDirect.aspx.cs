using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DBDirect : System.Web.UI.Page
{
    DataSet ds;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        //SiteValidation.ValidateSession();

            //if (Session["GroupID"].ToString() == "0" && ViewState["UserName"] != null && ViewState["UserName"].ToString() == "admin" && ViewState["Password"] != null && ViewState["Password"].ToString() == "ngws")
            if (ViewState["UserName"] != null && ViewState["UserName"].ToString() == "admin" && ViewState["Password"] != null && ViewState["Password"].ToString() == "ngws")
            {
                PanelLogin.Visible = false;
                PanelContent.Visible = true;
            }
            else
            {
                PanelLogin.Visible = true;
                PanelContent.Visible = false;
            }
        

        btnQuery.Attributes.Add("onclick", "return Confirmation()");    
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";

        try
        {
            ds = new DBDirects().MyQuery(txtQuery.Text);
            /*
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
                gridResult.DataSource = dt;
                gridResult.DataBind();
                if (dt.Rows.Count <= 0)
                    lblMessage.Text = "No Records";
                else
                    lblMessage.Text = "";
            }
            else
            {
                lblMessage.Text = "Successfully updated";
            }
            */
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                dt = null; 
                dt = ds.Tables[i];
                GridView gview = new GridView();
                gview.ID = "gview" + i;
                gview.Visible = true;
                gview.DataSource = dt;
                gview.DataBind();

                ContPanel.Controls.Add(gview);
            }
        }
        catch (Exception excep)
        {
            lblMessage.Text = excep.Message;
            gridResult.DataSource = null;
            gridResult.DataBind();
        }
    }

    protected void btnCancel_Click1(object sender, EventArgs e)
    {
        txtQuery.Text = "";
        lblMessage.Text = "";
        gridResult.DataSource = null;
        gridResult.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ViewState["UserName"] = txtUserName.Text;
        ViewState["Password"] = txtPassword.Text;

        //if (Session["GroupID"].ToString()=="0" &&  ViewState["UserName"] != null && ViewState["UserName"].ToString() == "admin" && ViewState["Password"] != null && ViewState["Password"].ToString() == "ngws")
        if (ViewState["UserName"] != null && ViewState["UserName"].ToString() == "admin" && ViewState["Password"] != null && ViewState["Password"].ToString() == "ngws")
        {
            PanelLogin.Visible = false;
            PanelContent.Visible = true;
        }
        else
        {
            PanelLogin.Visible = true;
            PanelContent.Visible = false;
        }
    }
}
