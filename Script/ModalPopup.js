﻿// JScript File


       
        var maskpanel = function() {
            this.divobj;
            this.show = function() {
           
                if (!document.getElementById("xdivmasking")) 
                {
                    var divEle = document.createElement('div');
                    divEle.setAttribute("id", "xdivmasking");
                    document.body.appendChild(divEle);
                    var divSty = document.getElementById("xdivmasking").style;
                    divSty.position = "absolute"; divSty.top = "0px"; divSty.left = "0px";
                    divSty.zIndex = "46"; divSty.opacity = ".50"; divSty.backgroundColor = "#000";
                    divSty.filter = "alpha(opacity=50)";

                    var divFram = document.createElement('iframe');
                    divFram.setAttribute("id", "xmaskframe");
                    document.body.appendChild(divFram);
                    divSty = document.getElementById("xmaskframe").style;
                    divSty.position = "absolute"; divSty.top = "0px"; divSty.left = "0px";
                    divSty.zIndex = "45"; divSty.border = "none"; divSty.filter = "alpha(opacity=0)";
                }

                this.divobj = document.getElementById("xdivmasking");
                this.waitifrm = document.getElementById("xmaskframe");

                var dsh = document.documentElement.scrollHeight;
                var dch = document.documentElement.clientHeight;
                var dsw = document.documentElement.scrollWidth;
                var dcw = document.documentElement.clientWidth;

                var bdh = (dsh > dch) ? dsh : dch;
                var bdw = (dsw > dcw) ? dsw : dcw;

                this.waitifrm.style.height = this.divobj.style.height = bdh + 'px';
                this.waitifrm.style.width = this.divobj.style.width = bdw + 'px';
                this.waitifrm.style.display = this.divobj.style.display = "block";
            };
            this.hide = function() {
                this.waitifrm.style.display = this.divobj.style.display = "none";
            };
        }

        function viewmaskpaneldemo(id) {
            demomask = new maskpanel();
            demomask.show();
            var demodiv = document.getElementById(id);
            demodiv.style.display = "block";
            demodiv.style.top = (((document.documentElement.clientHeight / 2) + document.documentElement.scrollTop) - (demodiv.offsetHeight / 2)) + 'px';
            demodiv.style.left = (((document.documentElement.clientWidth / 2) + document.documentElement.scrollLeft) - (demodiv.offsetWidth / 2)) + 'px';

            //setTimeout("hidemaskpaneldemo()",5000);
        }
        function hidemaskpaneldemo(id) {
            document.getElementById(id).style.display = "none";
            demomask.hide();
        }

    